#!/bin/sh

# Set up a simple demo project

set -e

cd "`dirname "$0"`"

TARGET=/tmp/DemoProject-tmp

[ -d "$TARGET" ] && {
  echo "$TARGET already exists"
  exit 5
}

git init "$TARGET"

cd demo-project
tar cvf - . | {
cd "$TARGET"
tar xvf -
}

cd "$TARGET"
git add .
git commit -m 'Current trunk of a working project.'

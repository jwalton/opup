import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class IoUtilsTest
{
    @Test
    public void testCopying() throws IOException
    {
        Reader r = new StringReader("Test");
        Writer w = new StringWriter();
        IOUtils.copy(r, w);
        
        assertEquals("Test", w.toString());
    }
}

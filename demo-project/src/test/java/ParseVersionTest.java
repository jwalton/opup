import org.apache.maven.artifact.repository.layout.LegacyRepositoryLayout;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

import static org.junit.Assert.assertEquals;


public class ParseVersionTest
{
    @Test
    public void parseVersionFromString()
    {
        ArtifactVersion av = new DefaultArtifactVersion("1.2");
        assertEquals(1, av.getMajorVersion());
        assertEquals(2, av.getMinorVersion());
        assertEquals(0, av.getBuildNumber());
    }
    
    @Test
    public void useLegacyRepository()
    {
        LegacyRepositoryLayout layout = new LegacyRepositoryLayout();
        assertNotNull(layout.toString());
    }
}

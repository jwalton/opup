#!/bin/sh

if [ "$#" -ne 3 ]; then
  echo >&2 "Usage: $0 <git working directory> <build command> <log target directory>"
  exit 5
fi

WORKINGDIR="$1"
CMD="$2"
LOGDIR="$3"

set -e -x

cd "`dirname "$0"`"

BASE="`pwd`"

RESULT="${LOGDIR}/results"

[ -d "$LOGDIR" ] || mkdir "$LOGDIR"

: >"$LOGDIR/build-pass-fail"

echo "$@" >$RESULT
date >>$RESULT

cd "$WORKINGDIR"

MF="`mktemp opup-tmp.XXXXXXXXXX`"

git branch -a | grep -o 'opup/upgrade-.*' | while read b; do
  echo >>$RESULT "Branch: $b"

  git checkout $b

  if "$BASE/build-and-log.sh" "$LOGDIR" $CMD; then
   echo >>$RESULT "$b succeeded"
   echo >>$MF "$b"
  else
   echo >>$RESULT "$b failed"

   git bisect start
   git bisect bad "$b"
   git bisect good master
   git bisect run "$BASE/build-and-log.sh" "$LOGDIR" $CMD

   { echo "Broken by:"; git show -s bisect/bad; } >>$RESULT
   GOOD="`git rev-parse bisect/bad^`"
   echo >>$MF "$GOOD"
  fi

done

MERGE="`cat $MF`"

if [ -n "$MERGE" ]; then
  git rev-parse $MERGE >"${LOGDIR}/upgrade-refs"

  echo "Success. Merge: ${MERGE}"
#  git checkout master

#  git merge $MERGE

  git checkout master
fi

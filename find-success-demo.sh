#!/bin/sh

# Hardcoded suggestions for a demo

CMD="mvn clean package"

cd "`dirname "$0"`"
BASE="`pwd`"

./find-success.sh /tmp/DemoProject-tmp "$CMD" "$BASE/opup-log"

#!/bin/sh

set -e

LOGDIR="$1"
shift

COMMIT="`git rev-parse HEAD`"

if "$@" >"$LOGDIR"/build-log-$COMMIT.txt; then
  echo "$COMMIT,good" >>"$LOGDIR/build-pass-fail"
  exit 0
else
  echo "$COMMIT,bad" >>"$LOGDIR/build-pass-fail"
  exit 1
fi

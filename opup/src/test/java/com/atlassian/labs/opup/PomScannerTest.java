package com.atlassian.labs.opup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.junit.Test;

public class PomScannerTest
{
    @Test
    public void definedArtifactsKnowsAboutProcessedPoms() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("sample-pom.xml");

        PomScanner ps = new PomScanner();
        ps.process(in);

        ArrayList<String> defined = new ArrayList<String>(ps.getDefinedArtifacts());

        assertEquals(1, defined.size());
        assertEquals("group-defined-in-parent:artifact-id", defined.get(0));
    }


    @Test
    public void currentVersionsIncludeAllTypes() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("sample-pom.xml");

        PomScanner ps = new PomScanner();
        ps.process(in);

        ps.findCurrent(mock(ReportTarget.class));
        Map<String, ArtifactVersion> current = ps.getCurrentVersions();

        Map<String, ArtifactVersion> expected = new HashMap<String, ArtifactVersion>();

        expected.put("a:simple", new DefaultArtifactVersion("1"));
        expected.put("b:war-artifact:war", new DefaultArtifactVersion("2"));
        expected.put("c:specified-in-dependency-management", new DefaultArtifactVersion("3"));
        expected.put("d:specified-through-property", new DefaultArtifactVersion("4"));

        assertEquals(expected, current);
    }

    @Test
    public void propertyForVersionIsAvailable() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("sample-pom.xml");

        PomScanner ps = new PomScanner();
        ps.process(in);

        assertEquals("property.version", ps.getPropertyForArtifactVersion("d:specified-through-property"));
        assertNull(ps.getPropertyForArtifactVersion("a:simple"));
    }

    @Test
    public void expressionsDecomposedToPropertyName() throws Exception
    {
        PomScanner ps = new PomScanner();

        assertEquals("x", ps.propNameFromExpression("${x}"));
        assertEquals("x.y", ps.propNameFromExpression("${x.y}"));

        assertNull(ps.propNameFromExpression("${}"));
        assertNull(ps.propNameFromExpression("${x"));
        assertNull(ps.propNameFromExpression("${x}-dep"));
        assertNull(ps.propNameFromExpression("${x}-${y}"));
    }

    @Test
    public void currentVersionsIncludePropertiesInProfiles() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("sample-with-profile.xml");

        PomScanner ps = new PomScanner();
        ps.process(in);

        ps.findCurrent(mock(ReportTarget.class));
        Map<String, ArtifactVersion> current = ps.getCurrentVersions();

        Map<String, ArtifactVersion> expected = new HashMap<String, ArtifactVersion>();

        expected.put("d:specified-through-property", new DefaultArtifactVersion("1.0"));

        assertEquals(expected, current);
    }

    @Test
    public void projectVersionDependenciesAreIgnored() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("sample-with-project-version-dependency.xml");

        PomScanner ps = new PomScanner();
        ps.process(in);

        ps.findCurrent(mock(ReportTarget.class));
        Map<String, ArtifactVersion> current = ps.getCurrentVersions();

        assertEquals(Collections.emptyMap(), current);
    }

    @Test
    public void undefinedPropertiesDoNotCauseAnError() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("sample-pom-with-undefined-property.xml");

        PomScanner ps = new PomScanner();
        ps.process(in);

        ReportTarget reportTarget = mock(ReportTarget.class);

        ps.findCurrent(reportTarget);
        Map<String, ArtifactVersion> current = ps.getCurrentVersions();

        Map<String, ArtifactVersion> expected = new HashMap<String, ArtifactVersion>();

        expected.put("b:simple", new DefaultArtifactVersion("2"));

        assertEquals(expected, current);

        verify(reportTarget).undefinedProperty("a:simple", "undefined.version");
    }

    @Test
    public void expandedIgnoredUnknownProperties()
    {
        PomScanner ps = new PomScanner();

        assertEquals("artifact-${variable}", ps.expanded("artifact-${variable}"));
    }

    @Test
    public void expandedUsesKnownProperties()
    {
        PomScanner ps = new PomScanner();

        ps.addProperty("variable", "value");

        assertEquals("artifact-value-value", ps.expanded("artifact-${variable}-${variable}"));
    }

    @Test
    public void expandedUsesKnownPropertiesIfMultipleDefinitionsMatch()
    {
        PomScanner ps = new PomScanner();

        ps.addProperty("variable", "value");
        ps.addProperty("variable", "value");

        assertEquals("artifact-value-value", ps.expanded("artifact-${variable}-${variable}"));
    }

    @Test
    public void duplicatedPropertiesAreNotExpanded()
    {
        PomScanner ps = new PomScanner();

        ps.addProperty("variable", "value");
        ps.addProperty("variable", "value2");

        assertEquals("artifact-${variable}", ps.expanded("artifact-${variable}"));
    }
}

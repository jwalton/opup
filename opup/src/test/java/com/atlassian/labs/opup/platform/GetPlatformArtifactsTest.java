package com.atlassian.labs.opup.platform;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.atlassian.labs.opup.VerySimpleCache;

import org.apache.commons.io.IOUtils;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class GetPlatformArtifactsTest
{
    private Set<String> expected = new HashSet<String>(Arrays.asList(
            "aopalliance:aopalliance:jar:1.0",
            "asm:asm:jar:3.1",
            "biz.aQute:bndlib:jar:1.43.0-atlassian-1",
            "bouncycastle:bcprov-jdk14:jar:138",
            "cglib:cglib:jar:2.2",
            "cglib:cglib-nodep:jar:2.1_3",
            "com.atlassian.applinks:applinks-api:jar:3.6.1",
            "com.atlassian.applinks:applinks-host:jar:3.6.1",
            "com.atlassian.applinks:applinks-spi:jar:3.6.1",
            "com.atlassian.aui:auiplugin:jar:3.5.0",
            "com.atlassian.event:atlassian-event:jar:2.1.1",
            "com.atlassian.ip:atlassian-ip:jar:2.0",
            "com.atlassian.multitenant:multitenant-core:jar:1.0-m14",
            "com.atlassian.oauth:atlassian-oauth-admin-plugin:jar:1.3.2",
            "com.atlassian.oauth:atlassian-oauth-api:jar:1.3.2",
            "com.atlassian.oauth:atlassian-oauth-bridge:jar:1.3.2",
            "com.atlassian.oauth:atlassian-oauth-consumer-core:jar:1.3.2",
            "com.atlassian.oauth:atlassian-oauth-consumer-plugin:jar:1.3.2",
            "com.atlassian.oauth:atlassian-oauth-consumer-sal-plugin:jar:1.3.2",
            "com.atlassian.oauth:atlassian-oauth-consumer-spi:jar:1.3.2",
            "com.atlassian.oauth:atlassian-oauth-service-provider-plugin:jar:1.3.2",
            "com.atlassian.oauth:atlassian-oauth-service-provider-sal-plugin:jar:1.3.2",
            "com.atlassian.oauth:atlassian-oauth-service-provider-spi:jar:1.3.2",
            "com.atlassian.oauth:atlassian-oauth-shared:jar:1.3.2",
            "com.atlassian.plugins:atlassian-plugins-core:jar:2.10.0",
            "com.atlassian.plugins:atlassian-plugins-main:jar:2.10.0",
            "com.atlassian.plugins:atlassian-plugins-osgi:jar:2.10.0",
            "com.atlassian.plugins:atlassian-plugins-osgi-events:jar:2.10.0",
            "com.atlassian.plugins:atlassian-plugins-servlet:jar:2.10.0",
            "com.atlassian.plugins:atlassian-plugins-spring:jar:2.10.0",
            "com.atlassian.plugins:atlassian-plugins-webfragment:jar:2.10.0",
            "com.atlassian.plugins:atlassian-plugins-webresource:jar:2.10.0",
            "com.atlassian.plugins:pacs-client:jar:1.1.28.1",
            "com.atlassian.plugins:pacs-shared:jar:1.1.28.1",
            "com.atlassian.plugins.rest:atlassian-rest-common:jar:2.5.0",
            "com.atlassian.plugins.rest:atlassian-rest-module:jar:2.5.0",
            "com.atlassian.plugins.rest:com.atlassian.jersey-library:pom:2.5.0",
            "com.atlassian.sal:sal-api:jar:2.7.0",
            "com.atlassian.sal:sal-core:jar:2.7.0",
            "com.atlassian.sal:sal-spi:jar:2.7.0",
            "com.atlassian.sal:sal-test-resources:jar:2.7.0",
            "com.atlassian.security:atlassian-secure-random:jar:1.0",
            "com.atlassian.security.auth.trustedapps:atlassian-trusted-apps-core:jar:2.5",
            "com.atlassian.security.auth.trustedapps:atlassian-trusted-apps-seraph-integration:jar:2.5",
            "com.atlassian.seraph:atlassian-seraph:jar:2.1.7-rc2",
            "com.atlassian.streams:streams-aggregator-plugin:jar:5.0",
            "com.atlassian.streams:streams-api:jar:5.0",
            "com.atlassian.streams:streams-core-plugin:jar:5.0",
            "com.atlassian.streams:streams-inline-actions-plugin:jar:5.0",
            "com.atlassian.streams:streams-spi:jar:5.0",
            "com.atlassian.templaterenderer:atlassian-template-renderer-api:jar:1.3.1",
            "com.atlassian.templaterenderer:atlassian-template-renderer-velocity-common:jar:1.3.1",
            "com.atlassian.templaterenderer:atlassian-template-renderer-velocity16-plugin:jar:1.3.1",
            "com.atlassian.upm:atlassian-universal-plugin-manager-plugin:jar:1.5.1",
            "com.atlassian.upm:spi:jar:1.5.1",
            "com.atlassian.util.concurrent:atlassian-util-concurrent:jar:2.3.0-beta2",
            "com.atlassian.velocity.htmlsafe:velocity-htmlsafe:jar:1.1.beta1",
            "com.google.collections:google-collections:jar:1.0",
            "com.sun.jersey:jersey-client:jar:1.0.3-atlassian-1",
            "com.sun.jersey:jersey-core:jar:1.0.3-atlassian-1",
            "com.sun.jersey:jersey-json:jar:1.0.3-atlassian-1",
            "com.sun.jersey:jersey-server:jar:1.0.3-atlassian-1",
            "com.sun.jersey.contribs:jersey-apache-client:jar:1.0.2",
            "com.sun.xml.bind:jaxb-impl:jar:2.1.10",
            "commons-codec:commons-codec:jar:1.3",
            "commons-collections:commons-collections:jar:3.2",
            "commons-fileupload:commons-fileupload:jar:1.2.1",
            "commons-httpclient:commons-httpclient:jar:3.1",
            "commons-io:commons-io:jar:1.4",
            "commons-lang:commons-lang:jar:2.4",
            "commons-logging:commons-logging:jar:1.0.4",
            "dom4j:dom4j:jar:1.4",
            "javax.activation:activation:jar:1.1",
            "javax.servlet:servlet-api:jar:2.5",
            "javax.validation:validation-api:jar:1.0.0.GA",
            "javax.ws.rs:jsr311-api:jar:1.0",
            "javax.xml.bind:jaxb-api:jar:2.1",
            "javax.xml.stream:stax-api:jar:1.0-2",
            "joda-time:joda-time:jar:1.6",
            "log4j:log4j:jar:1.2.7",
            "net.jcip:jcip-annotations:jar:1.0",
            "net.oauth.core:oauth:jar:20090617",
            "net.oauth.core:oauth-provider:jar:20090531",
            "net.sourceforge.nekohtml:nekohtml:jar:1.9.12",
            "org.apache.felix:org.apache.felix.bundlerepository:jar:1.4.0",
            "org.apache.felix:org.apache.felix.framework:jar:3.0.2",
            "org.apache.felix:org.apache.felix.shell:jar:1.2.0",
            "org.apache.sling:org.apache.sling.commons.osgi:jar:2.0.4-incubator",
            "org.apache.xmlgraphics:batik-css:jar:1.7",
            "org.apache.xmlgraphics:batik-ext:jar:1.7",
            "org.apache.xmlgraphics:batik-util:jar:1.7",
            "org.codehaus.jackson:jackson-core-asl:jar:1.4.4",
            "org.codehaus.jackson:jackson-jaxrs:jar:1.4.4",
            "org.codehaus.jackson:jackson-mapper-asl:jar:1.4.4",
            "org.codehaus.jackson:jackson-xc:jar:1.4.4",
            "org.codehaus.jettison:jettison:jar:1.0.1",
            "org.hibernate:hibernate-validator:jar:4.0.2.GA",
            "org.json:json:jar:20090211",
            "org.owasp.antisamy:antisamy:jar:1.4.4",
            "org.slf4j:slf4j-api:jar:1.5.8",
            "org.springframework:spring-aop:jar:2.5.6.SEC02",
            "org.springframework:spring-beans:jar:2.5.6.SEC02",
            "org.springframework:spring-context:jar:2.5.6.SEC02",
            "org.springframework:spring-core:jar:2.5.6.SEC02",
            "org.springframework:spring-web:jar:2.5.6.SEC02",
            "org.twdata.pkgscanner:package-scanner:jar:0.9.5",
            "xerces:xercesImpl:jar:2.8.1"
    ));

    @Ignore("This test hits the network.")
    @Test
    public void getsExpectedArtifacts() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("atlassian-platform-2.13.0.pom");
        assertNotNull(in);

        Set<String> actual = GetPlatformArtifacts.getPlatformDepsFrom(in);

        assertEquals(expected, actual);
    }

    @Test
    public void getsExpectedArtifactsFromFile() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("dependency-resolve.txt");
        assertNotNull(in);

        Set<String> actual = GetPlatformArtifacts.parseDeps(new InputStreamReader(in, "us-ascii"));

        assertEquals(expected, actual);
    }

    @Test
    @Ignore
    public void getsPlatformPomFromNetwork() throws Exception
    {
        VerySimpleCache vsc = new VerySimpleCache(new File("target/cache-" + Math.random()), new BasicCredentialsProvider());

        String s = IOUtils.toString(GetPlatformArtifacts.getPomForPlatformVersion(vsc, "2.13.0"));

        assertTrue(s.contains("Atlassian Integration Platform POM"));
        assertTrue(s.contains("<version>2.13.0</version>"));
    }

    @Test
    public void targetFromDeps()
    {
        Map<String, ArtifactVersion> expected = new HashMap<String, ArtifactVersion>();

        expected.put("a:b", new DefaultArtifactVersion("1"));
        expected.put("a:other", new DefaultArtifactVersion("2.0"));

        Map<String, ArtifactVersion> upgradeTarget = GetPlatformArtifacts.upgradeTargetFrom(Arrays.asList("a:b:jar:1", "a:other:jar:2.0"));

        assertEquals(expected, upgradeTarget);
    }
}

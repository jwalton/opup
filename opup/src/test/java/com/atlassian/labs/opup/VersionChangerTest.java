package com.atlassian.labs.opup;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.IOUtils;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.w3c.dom.Document;

import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class VersionChangerTest
{
    @Test
    public void returnsFalseWhenNoVersionsAreChanged() throws Exception
    {
        VersionChanger vc = new VersionChanger();

        InputStream in = getClass().getResourceAsStream("sample-pom.xml");

        assertFalse(vc.process(in, new ByteArrayOutputStream()));
    }

    private Document processSampleDocument(VersionChanger vc) throws Exception
    {
        InputStream in = getClass().getResourceAsStream("sample-pom.xml");

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        assertTrue(vc.process(in, baos));

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document after = dbf.newDocumentBuilder().parse(new ByteArrayInputStream(baos.toByteArray()));

        assertEquals("project", after.getDocumentElement().getTagName());
        assertEquals("http://maven.apache.org/POM/4.0.0", after.getDocumentElement().getNamespaceURI());

        return after;
    }

    private static String xpathEvaluateString(Document doc, String expr) throws XPathExpressionException
    {
        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(new TestNamespaceContext());

        return (String) xpath.evaluate(expr, doc, XPathConstants.STRING);
    }

    @Test
    public void simpleVersionsAreChanged() throws Exception
    {
        VersionChanger vc = new VersionChanger();
        vc.setNewVersion("a:simple", new DefaultArtifactVersion("1.1"));

        Document after = processSampleDocument(vc);
        assertEquals("1.1", xpathEvaluateString(after, "/m:project/m:dependencies/m:dependency[m:artifactId='simple']/m:version"));
    }

    @Test
    public void warVersionsAreChanged() throws Exception
    {
        VersionChanger vc = new VersionChanger();
        vc.setNewVersion("b:war-artifact:war", new DefaultArtifactVersion("2.1"));

        Document after = processSampleDocument(vc);
        assertEquals("2.1", xpathEvaluateString(after, "/m:project/m:dependencies/m:dependency[m:artifactId='war-artifact']/m:version"));
    }

    @Test
    public void dependencyManagementVersionsAreChanged() throws Exception
    {
        VersionChanger vc = new VersionChanger();
        vc.setNewVersion("c:specified-in-dependency-management", new DefaultArtifactVersion("3.1"));

        Document after = processSampleDocument(vc);
        assertEquals("3.1", xpathEvaluateString(after, "/m:project/m:dependencyManagement/m:dependencies/m:dependency[m:artifactId='specified-in-dependency-management']/m:version"));
    }

    @Test
    public void propertyVersionsAreChanged() throws Exception
    {
        VersionChanger vc = new VersionChanger();
        vc.setNewPropertyValue("property.version", new DefaultArtifactVersion("4.1"));

        Document after = processSampleDocument(vc);
        assertEquals("4.1", xpathEvaluateString(after, "/m:project/m:properties/m:property.version"));
    }

    @Test
    public void allVersionsAreChanged() throws Exception
    {
        VersionChanger vc = new VersionChanger();

        vc.setNewVersion("a:simple", new DefaultArtifactVersion("1.1"));
        vc.setNewVersion("b:war-artifact:war", new DefaultArtifactVersion("2.1"));
        vc.setNewVersion("c:specified-in-dependency-management", new DefaultArtifactVersion("3.1"));
        vc.setNewPropertyValue("property.version", new DefaultArtifactVersion("4.1"));

        Document after = processSampleDocument(vc);

        assertEquals("1.1", xpathEvaluateString(after, "/m:project/m:dependencies/m:dependency[m:artifactId='simple']/m:version"));
        assertEquals("2.1", xpathEvaluateString(after, "/m:project/m:dependencies/m:dependency[m:artifactId='war-artifact']/m:version"));
        assertEquals("3.1", xpathEvaluateString(after, "/m:project/m:dependencyManagement/m:dependencies/m:dependency[m:artifactId='specified-in-dependency-management']/m:version"));
        assertEquals("4.1", xpathEvaluateString(after, "/m:project/m:properties/m:property.version"));
    }

    static class TestNamespaceContext implements NamespaceContext
    {
        @Override
        public String getNamespaceURI(String prefix)
        {
            if (prefix.equals("m"))
            {
                return "http://maven.apache.org/POM/4.0.0";
            }
            else
            {
                return null;
            }
        }

        @Override
        public String getPrefix(String namespaceURI)
        {
            throw new UnsupportedOperationException();
        }

        @Override
        public Iterator<String> getPrefixes(String namespaceURI)
        {
            throw new UnsupportedOperationException();
        }
    }

    @Test
    public void reasonableStringRepresentationWhenEmpty() throws Exception
    {
        VersionChanger vc = new VersionChanger();
        assertTrue(vc.isEmpty());
        assertEquals("No changes.", vc.toString());
    }

    @Test
    public void reasonableStringRepresentationWithVersions() throws Exception
    {
        VersionChanger vc = new VersionChanger();

        vc.setNewVersion("group:artifact", new DefaultArtifactVersion("1"));

        assertFalse(vc.isEmpty());
        assertEquals("group:artifact 1", vc.toString());
    }

    @Test
    public void reasonableStringRepresentationWithProperties() throws Exception
    {
        VersionChanger vc = new VersionChanger();

        vc.setNewPropertyValue("version.property", new DefaultArtifactVersion("1"));

        assertFalse(vc.isEmpty());
        assertEquals("version.property 1", vc.toString());
    }

    @Test
    public void reasonableStringRepresentationWithMultipleVersions() throws Exception
    {
        VersionChanger vc;
        vc = new VersionChanger();

        vc.setNewVersion("c", new DefaultArtifactVersion("3"));
        vc.setNewVersion("d", new DefaultArtifactVersion("4"));

        assertTrue(vc.toString().equals("c 3, d 4") || vc.toString().equals("d 4, c 3"));
    }

    @Test
    public void reasonableStringRepresentationWithMultipleProperties() throws Exception
    {
        VersionChanger vc;
        vc = new VersionChanger();
        vc.setNewPropertyValue("a", new DefaultArtifactVersion("1"));
        vc.setNewPropertyValue("b", new DefaultArtifactVersion("2"));

        assertTrue(vc.toString().equals("a 1, b 2") || vc.toString().equals("b 2, a 1"));
    }

    @Test
    public void processedDocumentHasNewlineBeforeProject() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("sample-pom.xml");

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        VersionChanger vc = new VersionChanger();
        vc.process(in, baos);

        String result = new String(baos.toByteArray(), "utf-8");

        assertThat(result, CoreMatchers.<String>either(startsWith("<project")).or(Matchers.containsString("\n<project")));
    }

    @Test
    public void xmlFormattingSurvivesRoundTrip() throws Exception
    {
        InputStream origIn = getClass().getResourceAsStream("sample-pom.xml");

        String original = IOUtils.toString(origIn);

        VersionChanger vc = new VersionChanger();

        InputStream in;
        ByteArrayOutputStream out;

        vc.setNewPropertyValue("property.version", new DefaultArtifactVersion("1"));
        in = getClass().getResourceAsStream("sample-pom.xml");
        out = new ByteArrayOutputStream();
        vc.process(in, out);

        vc.setNewPropertyValue("property.version", new DefaultArtifactVersion("4"));
        in = new ByteArrayInputStream(out.toByteArray());
        out = new ByteArrayOutputStream();
        vc.process(in, out);

        String after = new String(out.toByteArray(), "us-ascii");

        assertEquals(original, after);
    }

    @Test
    public void addAllCombinesChangers()
    {
        VersionChanger vc1 = new VersionChanger();
        vc1.setNewPropertyValue("prop", new DefaultArtifactVersion("1"));

        VersionChanger vc2 = new VersionChanger();
        vc2.setNewVersion("group:artifact", new DefaultArtifactVersion("2"));

        VersionChanger combined = new VersionChanger();
        combined.addAll(vc1);
        combined.addAll(vc2);
        assertEquals("group:artifact 2, prop 1", combined.toString());
    }
}

package com.atlassian.labs.opup;

import java.io.IOException;
import java.io.StringReader;

import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

import static org.junit.Assert.assertFalse;

public class VersionOverridesTest
{
    @Test
    public void emptyOverridesRejectsNothing()
    {
        VersionOverrides vo = new VersionOverrides();
        assertTrue(vo.isValid("group:artifact", new DefaultArtifactVersion("0")));
    }

    @Test
    public void rejectOverridesByPattern() throws IOException
    {
        VersionOverrides vo = new VersionOverrides().load(new StringReader("group:artifact:1"));
        assertFalse(vo.isValid("group:artifact", new DefaultArtifactVersion("1")));
        assertTrue(vo.isValid("group:artifact", new DefaultArtifactVersion("2")));
    }

    @Test
    public void numericFinalReleasesAreRecognised()
    {
        assertTrue(VersionOverrides.isFinal(new DefaultArtifactVersion("1")));
        assertTrue(VersionOverrides.isFinal(new DefaultArtifactVersion("1.0")));
        assertTrue(VersionOverrides.isFinal(new DefaultArtifactVersion("1.0.0")));
    }

    @Test
    public void numericReleasesWithBuildNumbersAreFinal()
    {
        assertTrue(VersionOverrides.isFinal(new DefaultArtifactVersion("1.0.0-1")));
    }

    @Test
    public void finalReleasesWithWordsAreFinal()
    {
        assertTrue(VersionOverrides.isFinal(new DefaultArtifactVersion("2.0.5.RELEASE")));
        assertTrue(VersionOverrides.isFinal(new DefaultArtifactVersion("3.6.6.Final")));
    }

    @Test
    public void finalReleasesWithDifferentCaseWordsAreFinal()
    {
        assertTrue(VersionOverrides.isFinal(new DefaultArtifactVersion("2.0.5.release")));
        assertTrue(VersionOverrides.isFinal(new DefaultArtifactVersion("3.6.6.FINAL")));
    }

    @Test
    public void finalReleasesWithSubstringsAreNotFinal()
    {
        assertFalse(VersionOverrides.isFinal(new DefaultArtifactVersion("2.0.5.xrelease")));
        assertFalse(VersionOverrides.isFinal(new DefaultArtifactVersion("3.6.6.FINALZ")));
    }

    @Test
    public void releaseCandidatesAreNotFinal()
    {
        assertFalse(VersionOverrides.isFinal(new DefaultArtifactVersion("2.2.0-rc2")));
        assertFalse(VersionOverrides.isFinal(new DefaultArtifactVersion("2.0.1-rc3-20110120")));
        assertFalse(VersionOverrides.isFinal(new DefaultArtifactVersion("4.0.0.Beta1")));
    }

    @Test
    public void snapshotsAreNotFinal()
    {
        assertFalse(VersionOverrides.isFinal(new DefaultArtifactVersion("1-SNAPSHOT")));
    }

    @Test
    public void requireFinalEnforcesFinalVersions()
    {
        VersionOverrides vo = new VersionOverrides();
        assertTrue(vo.isValid("", new DefaultArtifactVersion("1.0.0-rc1")));
        vo = vo.requireFinal(true);
        assertFalse(vo.isValid("", new DefaultArtifactVersion("1.0.0-rc1")));
    }
}

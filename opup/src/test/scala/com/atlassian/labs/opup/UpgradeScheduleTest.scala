package com.atlassian.labs.opup;

import java.io.IOException
import java.util.Arrays
import java.util.Collections
import java.util.HashMap
import java.util.List

import scala.collection.JavaConverters.asScalaBufferConverter
import scala.collection.JavaConverters.seqAsJavaListConverter

import org.apache.maven.artifact.versioning.ArtifactVersion
import org.apache.maven.artifact.versioning.DefaultArtifactVersion
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class UpgradeScheduleTest
{
    def versions(versionStrings: String*) = versionStrings.map(new DefaultArtifactVersion(_): ArtifactVersion).asJava

    @Test
    def emptyUpgradePathsDiscarded()
    {
        val newVersions = new HashMap[String, List[ArtifactVersion]]();
        val newProperties = new HashMap[String, List[ArtifactVersion]]();

        newVersions.put("a", versions());
        newVersions.put("b", versions("1"));

        newProperties.put("c", versions());
        newProperties.put("d", versions("2"));

        var s = new UpgradeSchedule(newVersions, newProperties);

        s = s.withUpgrades();

        assertEquals(Collections.singleton("b"), s.getNewVersions().keySet());
        assertEquals(Collections.singleton("d"), s.getNewProperties().keySet());
    }

    @Test
    def latestStrategyMeanLatest()
    {
        val knownVersions = versions("1", "2").asScala.toList

        val aims = UpgradeSchedule.aimedFor("a:b", knownVersions, UpgradeStrategy.LATEST);

        assertEquals(knownVersions, aims);
    }

    @Test
    def specificAims()
    {
        val knownVersions = versions("1", "2").asScala.toList

        val upgradeTarget = Map("a:b" -> new DefaultArtifactVersion("1"));

        val aims = UpgradeSchedule.aimedFor("a:b", knownVersions, UpgradeStrategy.forVersions(upgradeTarget));
        assertEquals(scala.List(new DefaultArtifactVersion("1")), aims);

        val aims2 = UpgradeSchedule.aimedFor("a:other-artifact", knownVersions, UpgradeStrategy.forVersions(upgradeTarget));
        assertEquals(scala.List(), aims2);
    }

    @Test
    def artifactWithNoUpgradeTargetIsNotUpgraded()
    {
        val knownVersions = versions("1", "2").asScala.toList

        val aims = UpgradeSchedule.aimedFor("a:other-artifact", knownVersions, UpgradeStrategy.forVersions(Map()))
        assertEquals(scala.List(), aims)
    }

    @Test
    def incrementalUpgradeOnlyConsidersPatchReleases()
    {
        val knownVersions = versions("0.9", "1.0.1", "1.0.2", "1.1.0", "2").asScala.toList

        val current = Map("a:artifact" -> new DefaultArtifactVersion("1"))

        val aims = UpgradeSchedule.aimedFor("a:artifact", knownVersions, UpgradeStrategy.incremental(current));
        assertEquals(versions("1.0.1", "1.0.2").asScala,
                aims);
    }

    @Test
    def minorUpgradeOnlyConsidersMinorReleases()
    {
        val knownVersions = versions("0.9", "1.0.1", "1.0.2", "1.1.0", "2").asScala.toList

        val current = Map("a:artifact" -> new DefaultArtifactVersion("1.0.2"))

        val aims = UpgradeSchedule.aimedFor("a:artifact", knownVersions, UpgradeStrategy.minor(current));
        assertEquals(scala.List(new DefaultArtifactVersion("1.0.2"), new DefaultArtifactVersion("1.1.0")),
                aims);
    }

    // More a test of UpgradeStrategy, but keep it here until UpgradeStrategyTest moves over to Scala
    @Test
    def groupIdRootStrategyBehaves()
    {
        val check = UpgradeStrategy.withGroupIdRoot("test")

        assertFalse("No match", check("invalid", new DefaultArtifactVersion("1")))
        assertFalse("No artifactId", check("test", new DefaultArtifactVersion("1")))
        assertTrue("Okay", check("test:artifact", new DefaultArtifactVersion("1")))
        assertFalse("Group ID doesn't match", check("testx:artifact", new DefaultArtifactVersion("1")))
        assertTrue("Okay", check("test.subdomain:artifact", new DefaultArtifactVersion("1")))
    }

    // More a test of UpgradeStrategy, but keep it here until UpgradeStrategyTest moves over to Scala
    @Test
    def concatenatedStrategiesMeanIntersection()
    {
        val current = Map("test.group:artifact" -> new DefaultArtifactVersion("1.0.2"))

        val check = new UpgradeStrategy(null).getUpgradeTarget("incremental,groupIdRoot-test", current)

        /* Same as for groupIdRoot-test */
        assertFalse("No match", check("invalid", new DefaultArtifactVersion("1.0.3")))
        assertFalse("No artifactId", check("test", new DefaultArtifactVersion("1.0.3")))
        assertTrue("Okay", check("test.group:artifact", new DefaultArtifactVersion("1.0.3")))
        assertFalse("Group ID doesn't match", check("testx:artifact", new DefaultArtifactVersion("1.0.3")))

        /* Not incremental */
        assertFalse(check("test.group:artifact", new DefaultArtifactVersion("1.1")))
        assertFalse("Okay", check("test.group.subdomain:artifact", new DefaultArtifactVersion("1.1")))
    }
}

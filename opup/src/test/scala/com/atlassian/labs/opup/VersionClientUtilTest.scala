package com.atlassian.labs.opup

import org.apache.maven.artifact.versioning.DefaultArtifactVersion
import org.junit.Assert.assertEquals
import org.junit.Test

class VersionClientUtilTest
{
    @Test
    def sinceWhenCurrentVersionIsMissing()
    {
        val versions = List(new DefaultArtifactVersion("1.0"), new DefaultArtifactVersion("1.2"))

        assertEquals(List(new DefaultArtifactVersion("1.2")),
                VersionClientUtil.since(versions, new DefaultArtifactVersion("1.1")));
    }

    @Test
    def sinceWhenCurrentVersionIsPresent()
    {
        val versions = List(new DefaultArtifactVersion("1.0"), new DefaultArtifactVersion("1.2"))

        assertEquals(List(new DefaultArtifactVersion("1.2")),
                VersionClientUtil.since(versions, new DefaultArtifactVersion("1.0")));
    }
}

package com.atlassian.labs.opup

import java.io.StringReader
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito
import java.io.Reader
import java.io.IOException
import scala.util.control.Exception
import org.mockito.stubbing.Answer
import org.mockito.invocation.InvocationOnMock

class UtilsTest {
  @Test
  def emptyYieldsNothing() {
    assertEquals(List(), Utils.lines(new StringReader("")).toList)
  }

  @Test
  def linesAreReturned() {
    assertEquals(List("a", "b"), Utils.lines(new StringReader("a\nb")).toList)
  }

  @Test
  def closedOnError() {
    val r = Mockito.mock(classOf[Reader], new Answer[Object] {
      def answer(invocation: InvocationOnMock) = throw new IOException
    })

    Mockito.doNothing().when(r).close()

    Exception.ignoring(classOf[IOException]) {
      Utils.lines(r)
    }

    Mockito.verify(r).close()
  }

  @Test
  def whitespaceIsTrimmed() {
    assertEquals(List("a", "b"), Utils.lines(new StringReader(" a \nb ")).toList)
  }

  @Test
  def emptyLinesAreNotReturned() {
    assertEquals(List("a", "b"), Utils.lines(new StringReader("a\n\nb")).toList)
  }

  @Test
  def blankLinesAreNotReturned() {
    assertEquals(List("a", "b"), Utils.lines(new StringReader("a\n \nb")).toList)
  }

  @Test
  def commentedLinesAreNotReturned() {
    assertEquals(List("a", "b"), Utils.lines(new StringReader("a\n# Comment\nb")).toList)
  }
}

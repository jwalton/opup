package com.atlassian.labs.opup

import java.io.ByteArrayInputStream
import java.io.IOException
import org.apache.maven.artifact.versioning.DefaultArtifactVersion
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Matchers.anyString
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.Mockito.when
import java.io.FileNotFoundException


class NexusVersionClientTest {
    def v(s: String) = new DefaultArtifactVersion(s)

    @Test
    def parseVersionsFromSearchResults
    {
        val in = getClass().getResourceAsStream("https___m_proxy_atlassian_com_service_local_data_index_g_com_megginson_sax_a_xml_writer")

        val versions = new NexusVersionClient(null, null).getVersionsDataIndex(in, "")

        assertEquals(List((v("0.2"), "jar")), versions)
    }

    @Test
    def parseVersionsFromLuceneSearchResults
    {
        val in = getClass().getResourceAsStream("https___m_proxy_atlassian_com_service_local_lucene_search_g_com_megginson_sax_a_xml_writer")

        val versions = new NexusVersionClient(null, null).getVersionsLuceneSearch(in, "")

        assertEquals(List((v("0.2"), "jar")), versions)
    }

    @Test
    def versionsFromLuceneSearchResultsIncludesNonJarArtifacts
    {
        val in = getClass().getResourceAsStream("https___maven_atlassian_com_service_local_lucene_search_g_org_apache_tomcat_a_apache_tomcat")

        val versions = new NexusVersionClient(null, null).getVersionsLuceneSearch(in, "")

        assertEquals(List((v("8.0.9"), "zip")), versions)
    }

    @Test
    def parseVersionsIncludesSnapshots()
    {
        val in = getClass().getResourceAsStream("index-with-snapshot-artifact.xml")

        val versions = new NexusVersionClient(null, null).getVersionsDataIndex(in, "")

        assertEquals(List((v("1"), "jar"), (v("2-SNAPSHOT"), "jar")), versions)
    }

    @Test
    def argumentsAreUrlEncodedForLuceneSearch()
    {
      val mockCache = Mockito.mock(classOf[HttpCache])

      when(mockCache.get(anyString())).thenReturn(new ByteArrayInputStream("<x/>".getBytes("us-ascii")))

      val versions = new NexusVersionClient(mockCache, "dummy:")

      versions.getVersionsAndPackagingTypes("a&b", "c$d")

      verify(mockCache).get(anyString())
      verify(mockCache).get("dummy:/service/local/lucene/search?g=a%26b&a=c%24d")
    }

    @Test
    def argumentsAreUrlEncoded()
    {
      val mockCache = Mockito.mock(classOf[HttpCache])

      when(mockCache.get(anyString())).thenThrow(classOf[FileNotFoundException]).thenReturn(new ByteArrayInputStream("<x/>".getBytes("us-ascii")))

      val versions = new NexusVersionClient(mockCache, "dummy:")

      versions.getVersionsAndPackagingTypes("a&b", "c$d")

      verify(mockCache).get("dummy:/service/local/data_index?g=a%26b&a=c%24d")
    }
}

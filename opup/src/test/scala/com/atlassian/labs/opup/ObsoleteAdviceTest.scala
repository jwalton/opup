package com.atlassian.labs.opup;

import java.io.StringReader

import org.junit.Assert.assertEquals
import org.junit.Test

class ObsoleteAdviceTest
{
    @Test
    def nullByDefault()
    {
        val oa = new ObsoleteAdvice()
        assertEquals(None, oa.getAdvice(""))
    }

    @Test
    def adviceFromFile()
    {
        val oa = ObsoleteAdvice.load(new StringReader("a:b,replaced by c:d"))
        assertEquals(Some("replaced by c:d"), oa.getAdvice("a:b"))
    }

    @Test
    def genericAdviceWhenNothingSpecific()
    {
        val oa = ObsoleteAdvice.load(new StringReader("a:b"))
        assertEquals(Some("Obsolete"), oa.getAdvice("a:b"))
    }
}

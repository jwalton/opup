package com.atlassian.labs.opup

import org.junit.Test
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import java.io.File

class UpgradeBranchCreatorTest {
  @Test
  def relativisedPaths(): Unit = {
    val base = new File(".").getAbsoluteFile()

    val files = Seq(new File("test.txt"), new File(base, "test.txt"), new File(base, "sub/sub/test.txt"))

    val relative = UpgradeBranchCreator.relativise(base, files)
    assertEquals(Seq("test.txt", "test.txt", "sub/sub/test.txt"), relative)

    val relative2 = UpgradeBranchCreator.relativise(new File("."), files)
    assertEquals(Seq("test.txt", "test.txt", "sub/sub/test.txt"), relative2)
  }
}

package com.atlassian.labs.opup
import org.junit.Test
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.apache.maven.artifact.versioning.DefaultArtifactVersion
import org.hamcrest.Matchers

class JUnitReportTest {
  def v(x: String) = new DefaultArtifactVersion(x)

  @Test
  def sameVersionMeansNoFailure() {
    val dc = JUnitReport.asTestcase("", v("1"), v("1"))
    assertTrue(dc.child.isEmpty)
  }

  @Test
  def laterVersionAvailableCausesFailure() {
    val dc = JUnitReport.asTestcase("artifact-id", v("1"), v("1.1"))
    val failure = dc \ "failure"

    assertEquals(1, failure.length)
    assertThat(failure.text, Matchers.startsWith("Artifact artifact-id "))
  }

  @Test
  def dotsInArtifactIdentifierAreEscaped() {
    val dc = JUnitReport.asTestcase("artifact.version", v("1"), v("1"))

    assertEquals(Some("artifact_version"), dc.attribute("classname").map(_.text))
  }

  @Test
  def testNameIncludeArtifactIdentifier() {
    val dc = JUnitReport.asTestcase("artifact.version", v("1"), v("1"))

    assertEquals(Some("use_artifact_version_Version_1"), dc.attribute("name").map(_.text))
  }
}

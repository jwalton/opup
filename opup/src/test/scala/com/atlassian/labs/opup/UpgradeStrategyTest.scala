package com.atlassian.labs.opup

import java.io.IOException
import java.io.InputStream

import org.apache.http.Header
import org.apache.maven.artifact.versioning.DefaultArtifactVersion
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class UpgradeStrategyTest
{
    @Test(expected = classOf[IllegalArgumentException])
    def unknownPolicyThrowsException()
    {
        val up = new UpgradeStrategy(null);
        up.getUpgradeTarget("unknown", null);
    }

    @Test(expected = classOf[IllegalArgumentException])
    def unknownPolicyThatMatchesSuffixThrowsException()
    {
        val up = new UpgradeStrategy(null);
        up.getUpgradeTarget("prefix-platform-1", null);
    }

    @Test
    def latestIsASpecificTarget()
    {
        val up = new UpgradeStrategy(null);

        assertEquals(UpgradeStrategy.LATEST, up.getUpgradeTarget("latest", null));
    }

    @Test
    def platformAttemptsToUseCache()
    {
        val arr = Array(false)

        val cache = new HttpCache()
        {
            def get(url: String): InputStream =
            {
                arr(0) = true
                throw new IOException()
            }
            override def get(url: String, headers: Array[Header]): InputStream =
            {
                arr(0) = true
                throw new IOException()
            }
        }

        val up = new UpgradeStrategy(cache);
        try
        {
            up.getUpgradeTarget("platform-1", null);
        }
        catch
        {
          case e: IOException => {
            assertTrue(e.isInstanceOf[IOException])
            assertTrue(arr(0))
          }
        }
    }

    @Test
    def incrementalUpgradeStrategyWillIncreasePatchVersion()
    {
        val current = Map("library" -> new DefaultArtifactVersion("1.2.3"))

        /* Upgrade */
        assertTrue(UpgradeStrategy.incremental(current).apply("library", new DefaultArtifactVersion("1.2.4")))

        /* Or stay where you are */
        assertTrue(UpgradeStrategy.incremental(current).apply("library", new DefaultArtifactVersion("1.2.3")))

        /* Don't downgrade */
        assertFalse(UpgradeStrategy.incremental(current).apply("library", new DefaultArtifactVersion("1.2.2")))

        /* Or bump the major or minor version */
        assertFalse(UpgradeStrategy.incremental(current).apply("library", new DefaultArtifactVersion("1.3.4")))
        assertFalse(UpgradeStrategy.incremental(current).apply("library", new DefaultArtifactVersion("2.3.4")))
    }

    @Test
    def incrementalUpgradeStrategyIsNotConfusedByStringQualifiersWithDashes()
    {
      val v = new DefaultArtifactVersion("2.5.6-SEC03")
      val current = Map("library" -> v)

      assertFalse(UpgradeStrategy.incremental(current).apply("library", new DefaultArtifactVersion("4.1.4-RELEASE")))
    }

    /**
     * This "test" captures the current, rather than desired, behaviour: DefaultArtifactVersion
     * doesn't break down a version string with extra dot-separated components so
     * the incremental strategy incorrectly suggests major version bumps.
     */
    @Test
    def incrementalUpgradeStrategyIsConfusedByStringQualifiers()
    {
      val v = new DefaultArtifactVersion("2.5.6.SEC03")
      val current = Map("library" -> v)

      assertEquals(0, v.getMajorVersion)
      assertEquals(0, v.getMinorVersion)
      assertEquals(0, v.getIncrementalVersion)
      assertEquals("2.5.6.SEC03", v.getQualifier)

      assertTrue(UpgradeStrategy.incremental(current).apply("library", new DefaultArtifactVersion("4.1.4.RELEASE")))
    }
}

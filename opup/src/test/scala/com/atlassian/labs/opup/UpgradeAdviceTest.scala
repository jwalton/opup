package com.atlassian.labs.opup
import org.junit.Test
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.apache.maven.artifact.versioning.DefaultArtifactVersion

class UpgradeAdviceTest {
  @Test
  def loadingFromResources(): Unit = {
    val advice = UpgradeAdvice.fromResources()

    assertTrue(advice.obsoleteAdvice.getAdvice("quartz:quartz").isDefined)
    assertFalse(advice.obsoleteAdvice.getAdvice("no-such-group:no-such-artifact").isDefined)

    assertFalse(advice.versionOverrides.isValid("commons-lang:commons-lang", new DefaultArtifactVersion("20030203.000129")))
    assertTrue(advice.versionOverrides.isValid("no-such-group:no-such-artifact", new DefaultArtifactVersion("1")))
  }

  @Test
  def canRequireFinalVersions(): Unit = {
    var advice = UpgradeAdvice.fromResources()

    assertTrue(advice.versionOverrides.isValid("example:example", new DefaultArtifactVersion("1-rc1")))

    advice = advice.requireFinal(true)
    assertFalse(advice.versionOverrides.isValid("example:example", new DefaultArtifactVersion("1-rc1")))

    advice = advice.requireFinal(false)
    assertTrue(advice.versionOverrides.isValid("example:example", new DefaultArtifactVersion("1-rc1")))
  }
}

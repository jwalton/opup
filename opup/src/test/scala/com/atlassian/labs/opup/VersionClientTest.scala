package com.atlassian.labs.opup

import java.io.StringReader

import scala.collection.JavaConverters.seqAsJavaListConverter

import org.apache.maven.artifact.versioning.ArtifactVersion
import org.apache.maven.artifact.versioning.DefaultArtifactVersion
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.when

class VersionClientTest {
    def v(s: String) = new DefaultArtifactVersion(s)

    @Test
    def filteringIgnoresSnapshots()
    {
        val in = getClass().getResourceAsStream("index-with-snapshot-artifact.xml")

        val rt = mock(classOf[ReportTarget])

        val vc = mock(classOf[VersionClient])
        when(vc.getVersions("", "", "jar", rt)).thenReturn(
            List[ArtifactVersion](v("1"), v("1-SNAPSHOT")))

        val filtered = VersionClientUtil.filtered(vc, new VersionOverrides())

        val versions = filtered.getVersions("", "", "jar", rt);

        assertEquals(List(new DefaultArtifactVersion("1")), versions);
    }

    @Test
    def filteringIgnoresBlacklist()
    {
        val rt = mock(classOf[ReportTarget])

        val vc = mock(classOf[VersionClient])
        when(vc.getVersions("group", "artifact", "jar", rt)).thenReturn(
            List[ArtifactVersion](v("1")))

        val overrides = new VersionOverrides().load(new StringReader("group:artifact:1"))

        val filtered = VersionClientUtil.filtered(vc, overrides)

        val versions = filtered.getVersions("group", "artifact", "jar", rt);

        assertEquals(List(), versions);
    }

    @Test
    def bundlesAreAcceptedAsJars()
    {
        val rt = mock(classOf[ReportTarget])

        val vc = new PackagingFilteringVersionClient {
          def getVersionsAndPackagingTypes(groupId: String, artifactId: String) = List[(ArtifactVersion, String)](
              (v("1"), "bundle"),
              (v("2"), "jar"),
              (v("3"), "unknown-packaging-type")
            )
        }

        val versions = vc.getVersions("group", "artifact", "jar", rt);

        assertEquals(List(new DefaultArtifactVersion("1"), new DefaultArtifactVersion("2")), versions);
    }
}

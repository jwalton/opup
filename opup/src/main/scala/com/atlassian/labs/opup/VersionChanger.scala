package com.atlassian.labs.opup;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.IOUtils;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import scala.collection.JavaConverters._

class VersionChanger
{
    val docBuild = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    val identity = TransformerFactory.newInstance().newTransformer();
    identity.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

    val newVersions = new HashMap[String, ArtifactVersion]();
    val newProperties = new HashMap[String, ArtifactVersion]();

    private var key: String = _

    def setKey(k: String)
    {
        this.key = k;
    }

    def getKey() = key

    def setNewVersion(coords: String, version: ArtifactVersion) =
    {
        newVersions.put(PomScanner.groupAndArtifact(coords), version);
    }

    def setNewPropertyValue(name: String, version: ArtifactVersion) =
    {
        newProperties.put(name, version);
    }

    def process(in: InputStream, out: OutputStream): Boolean =
    {
        val doc = docBuild.parse(in);

        val changed = process(doc);

        val source = new DOMSource(doc);
        val target = new StreamResult(out);

        identity.transform(source, target);

        out.write('\n');

        return changed;
    }

    def elements(nl: NodeList) =
      Range(0, nl.getLength).map(nl.item(_)).collect{case e: Element => e}

    def process(doc: Document): Boolean =
    {
        var changed = false;

        val project = doc.getDocumentElement();

        for (d <- elements(project.getElementsByTagName("dependency")))
        {
            val groupId = getValue(d, "groupId");
            val artifactId = getValue(d, "artifactId");

            val currentVersion = getValue(d, "version");

            val nv = newVersions.get(groupId + ":" + artifactId);
            if (nv != null)
            {
                val nl2 = d.getElementsByTagName("version");
                if (nl2.getLength() > 0)
                {
                    nl2.item(0).setTextContent(nv.toString());

                    if (!nv.toString().equals(currentVersion))
                    {
                        changed = true;
                    }
                }
            }
        }

        for (props <- elements(project.getElementsByTagName("properties")))
        {
            val nl2 = props.getElementsByTagName("*");
            for (p <- elements(props.getElementsByTagName("*")))
            {
                val nv = newProperties.get(p.getTagName());
                if (nv != null)
                {
                    if (!p.getTextContent().equals(nv.toString()))
                    {
                        p.setTextContent(nv.toString());
                        changed = true;
                    }
                }
            }
        }

        return changed;
    }

    def getValue(e: Element, name: String): String =
    {
        val nl = e.getElementsByTagName(name);
        if (nl.getLength() > 0)
        {
            return nl.item(0).getTextContent();
        }
        else
        {
            return null;
        }
    }

    def changeAll(poms: Traversable[File]) =
    {
        for (pom <- poms)
        {
            val out = new ByteArrayOutputStream();

            var newVersion: File = null;

            val in = new FileInputStream(pom);
            try
            {
                if (process(in, out))
                {
                    val t = new File(pom.getPath() + ".opup-tmp");
                    val fout = new FileOutputStream(t);

                    try
                    {
                        IOUtils.copy(new ByteArrayInputStream(out.toByteArray()), fout);
                        newVersion = t;
                    }
                    finally
                    {
                        fout.close();
                    }
                }
            }
            finally
            {
                in.close();
            }

            if (newVersion != null)
            {
                if (!newVersion.renameTo(pom)) {
                    System.err.println("Failed to rename " + newVersion);
                }
            }
        }
    }

    def isEmpty(): Boolean =
    {
        return newVersions.isEmpty() && newProperties.isEmpty();
    }

    override def toString(): String =
    {
        val all = (newVersions.asScala.iterator ++ newProperties.asScala.iterator)

        val allStrings = (for ((k, v) <- all) yield {k + " " + v}).toList

        if (!allStrings.isEmpty)
        {
            return allStrings.mkString(", ")
        }
        else
        {
            return "No changes.";
        }
    }

    def addAll(vc: VersionChanger) = {
      newVersions.putAll(vc.newVersions)
      newProperties.putAll(vc.newProperties)
    }
}

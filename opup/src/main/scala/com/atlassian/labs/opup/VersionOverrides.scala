package com.atlassian.labs.opup;

import java.io.File
import java.io.FileReader
import java.io.Reader

import scala.collection.JavaConverters.asScalaBufferConverter

import org.apache.commons.io.IOUtils
import org.apache.maven.artifact.versioning.ArtifactVersion

class VersionOverrides private (invalid: Set[String], requireFinal: Boolean)
{
    def this() = this(Set(), false)

    def requireFinal(b: Boolean) = new VersionOverrides(invalid, b)

    def isValid(coords: String, version: ArtifactVersion): Boolean =
    {
        if (requireFinal && !VersionOverrides.isFinal(version))
        {
            return false;
        }

        return !invalid.contains(coords + ":" + version);
    }

    def load(r: Reader) = {
      val newExclusions = Utils.lines(r).toSet
      new VersionOverrides(invalid ++ newExclusions, requireFinal)
    }
}

object VersionOverrides {
  val WORDY_RELEASE =
    """(?i)\d+\.\d+\.\d\.(RELEASE|Final)""".r

  def isFinal(artifactVersion: ArtifactVersion) = {
    artifactVersion.getQualifier() == null ||
      WORDY_RELEASE.pattern.matcher(artifactVersion.toString()).matches()
  }
}

package com.atlassian.labs.opup

import java.io.File
import java.io.FileReader
import java.io.Reader

import scala.collection.JavaConverters.asScalaBufferConverter

import org.apache.commons.io.IOUtils

class ObsoleteAdvice(advice: Map[String, String]) {
    def this() = this(Map())
    def getAdvice(artifact: String) = advice.get(artifact)
}

object ObsoleteAdvice {
    def load(r: Reader): ObsoleteAdvice =
    {
        new ObsoleteAdvice(Utils.lines(r).map(s =>
        {
            val sa = s.split(",", 2);

            (sa.head, sa.lift(1).getOrElse("Obsolete"))
        }).toMap)
    }
}

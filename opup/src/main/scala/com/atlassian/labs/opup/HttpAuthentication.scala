package com.atlassian.labs.opup

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.Authenticator
import java.net.PasswordAuthentication

import org.apache.http.auth.AuthScope
import org.apache.http.auth.Credentials
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.client.CredentialsProvider

object HttpAuthentication {
  def hardcoded(auth: PasswordAuthentication): Authenticator = {
    return new Authenticator {
      override def getPasswordAuthentication(): PasswordAuthentication = auth
    }
  }

  def fromConsole = ConsolePasswordAuthenticator

  def anonymous = new Authenticator{}
}

object ConsolePasswordAuthenticator extends CredentialsProvider {
    def clear() {}
    def setCredentials(x: AuthScope, y: Credentials) {}

    def getCredentials(x: AuthScope): Credentials = {
                val con = System.console();

                if (con != null)
                {
                    con.printf("Maven repository requires authentication.\n");
                    val username = con.readLine("Username: ");
                    val password = new String(con.readPassword("Password: "))

                    new UsernamePasswordCredentials(username, password)
                }
                else
                {
                    try
                    {
                        /* Hello, Eclipse! */
                        System.out.print("Maven repository requires authentication.\n");
                        val br = new BufferedReader(new InputStreamReader(System.in));

                        System.out.println("Username: ");
                        val username = br.readLine();

                        System.out.println("Password: ");
                        val password = br.readLine()

                        new UsernamePasswordCredentials(username, password)
                    }
                    catch
                    {
                        case ioe: IOException => throw new RuntimeException("Unable to get username and password", ioe)
                    }
                }
            }

}

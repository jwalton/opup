package com.atlassian.labs.opup

import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import java.net.URLEncoder

import scala.Range

import org.apache.maven.artifact.versioning.ArtifactVersion
import org.apache.maven.artifact.versioning.DefaultArtifactVersion
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList

import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory


class NexusVersionClient(cache: HttpCache, nexusUrl: String) extends PackagingFilteringVersionClient {
    val docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

    def getVersionsAndPackagingTypes(groupId: String, artifactId: String): List[(ArtifactVersion, String)] =
    {
        val luceneUrl = nexusUrl + "/service/local/lucene/search?" +
                          "g=" + URLEncoder.encode(groupId, "utf-8") +
                          "&a=" + URLEncoder.encode(artifactId, "utf-8");

        val url = nexusUrl + "/service/local/data_index?" +
                          "g=" + URLEncoder.encode(groupId, "utf-8") +
                          "&a=" + URLEncoder.encode(artifactId, "utf-8");

        // p= packaging
        // c= classifier

        try {
          val in = cache.get(luceneUrl)

          try {
            getVersionsLuceneSearch(in, groupId + ":" + artifactId)
          } finally {
            in.close();
          }
        } catch {
          case _: FileNotFoundException => {
            val in = cache.get(url)

            try {
              getVersionsDataIndex(in, groupId + ":" + artifactId)
            } finally {
              in.close()
            }
          }
        }
    }

    val xpath = XPathFactory.newInstance().newXPath();

    // XXX Copied
    def evaluate(xp: String, d: Node) =
        xpath.evaluate(xp, d, XPathConstants.STRING).asInstanceOf[String]

    // XXX Copied
    def elements(nl: NodeList) =
      Range(0, nl.getLength).map(nl.item(_)).collect{case e: Element => e}

    def getVersionsDataIndex(xmlStream: InputStream, coords: String): List[(ArtifactVersion, String)] =
    {
        val doc = docBuilder.parse(xmlStream);

//        NodeList nl = (NodeList) xpath.evaluate("content/data/content-item[leaf='false']", doc, XPathConstants.NODESET);
        val nl = xpath.evaluate("search-results/data/artifact", doc, XPathConstants.NODESET).asInstanceOf[NodeList];

        val versions = collection.mutable.LinkedHashSet[(ArtifactVersion, String)]();

        for (e <- elements(nl))
        {
            // TODO Confirm it has the right classifier, packaging etc.

//            String text = (String) xpath.evaluate("text", e, XPathConstants.STRING);
            val text = evaluate("version", e);
            val packaging = evaluate("packaging", e);

            val dav = new DefaultArtifactVersion(text);

            versions += ((dav: ArtifactVersion, packaging))
        }

        return versions.toList
    }

    def getVersionsLuceneSearch(xmlStream: InputStream, coords: String): List[(ArtifactVersion, String)] =
    {
        val doc = docBuilder.parse(xmlStream);

        val nl = xpath.evaluate("searchNGResponse/data/artifact", doc, XPathConstants.NODESET).asInstanceOf[NodeList];

        val versions = collection.mutable.LinkedHashSet[(ArtifactVersion, String)]();

        for (e <- elements(nl)) {
          val version = evaluate("version", e)

          /* Exists in unclassified form? */
          for (al <- elements(xpath.evaluate("artifactHits/artifactHit/artifactLinks/artifactLink[not(classifier)]",
              e, XPathConstants.NODESET).asInstanceOf[NodeList])) {
            val extension = evaluate("extension", al)

            if (extension != "pom") {
              val dav = new DefaultArtifactVersion(version);
              versions += ((dav: ArtifactVersion, extension))
            }
          }
        }

        return versions.toList
    }
}

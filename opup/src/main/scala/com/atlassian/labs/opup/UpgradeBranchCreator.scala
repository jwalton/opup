package com.atlassian.labs.opup

import org.eclipse.jgit.api.Git
import scala.collection.JavaConverters._
import org.eclipse.jgit.lib.ObjectId
import java.io.File

class UpgradeBranchCreator(git: Git) {
    val BRANCH_PREFIX = "opup/"

    def checkout(branch: String): Unit = {
      git.checkout().setName(branch).call()
    }

    def deleteAllOpupBranches(log: ((String) => Unit)) =
    {
        val branches = git.branchList().call();
        for (r <- branches.asScala)
        {
            log("Exists: " + r.getName());

            if (r.getName().startsWith("refs/heads/" + BRANCH_PREFIX))
            {
                git.branchDelete().setBranchNames(r.getName()).setForce(true).call();
            }
        }
    }

    def createBranches(allChanges: collection.Map[String, Seq[VersionChanger]], poms: Traversable[File],
        log: ((String) => Unit)): Map[ObjectId, VersionChanger] =
    {
        val totalBranches = allChanges.size
        var done = 0

        val commits = collection.mutable.Map[ObjectId, VersionChanger]()

//        var commitKeys = collection.mutable.ListBuffer[String]()

        val patterns = UpgradeBranchCreator.relativise(git.getRepository().getWorkTree(), poms)

        for ((name, changers) <- allChanges)
        {
            val branchName = BRANCH_PREFIX + "upgrade-" + name;

            val ref = git.checkout().setStartPoint("master").setCreateBranch(true).setForce(true).setName(branchName).call();

            for (changer <- changers)
            {
                changer.changeAll(poms)

                val dc = patterns.foldLeft(git.add())(_.addFilepattern(_)).call()

                for (f <- patterns) {
                  if (dc.getEntry(f) == null) {
                    throw new RuntimeException("Failed to include file in commit: " + f)
                  }
                }

                val cmt = git.commit().setMessage("Upgrade: " + changer.toString()).call()

                commits.put(cmt.toObjectId(), changer)
            }

            done += 1;

            log("Branches: " + done + "/" + totalBranches + ": " + branchName);
        }

        return commits.toMap
    }
}

object UpgradeBranchCreator {
  def relativise(base: File, files: Traversable[File]) = {
    val absBase = base.getCanonicalPath + File.separator

    base.getCanonicalPath()

    files.map((f: File) => {
      val p = f.getCanonicalPath
      if (p.startsWith(absBase)) {
        p.substring(absBase.length)
      } else {
        p
      }
    })
  }
}

package com.atlassian.labs.opup.platform;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.atlassian.labs.opup.HttpCache;

import org.apache.commons.io.IOUtils;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.w3c.dom.Element;

import scala.collection.JavaConverters._

object GetPlatformArtifacts
{
    val MAVEN_NS = "http://maven.apache.org/POM/4.0.0"

    def getPlatformDepsFrom(cache: HttpCache, version: String): Set[String] = 
    {
        val pomInput = getPomForPlatformVersion(cache, version);

        return(getPlatformDepsFrom(getPlatformDepsAsTextFile(pomInput)))
    }

    def getPlatformDepsFrom(in: InputStream):  Set[String] = getPlatformDepsFrom(getPlatformDepsAsTextFile(in))

    def getPlatformDepsFrom(f: File): Set[String] =
    {
        val r = new FileReader(f);
        try
        {
            return parseDeps(r);
        }
        finally
        {
            r.close();
        }
    }
    
    def getPomForPlatformVersion(cache: HttpCache, version: String): InputStream =
    {
        val pomUrl = "https://packages.atlassian.com/maven/public/com/atlassian/platform/platform-poms/" + version + "/platform-poms-" + version + ".pom"
        return cache.get(pomUrl)
    }

    def getPlatformDepsAsTextFile(in: InputStream): File =
    {
        val dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);

        val db = dbf.newDocumentBuilder();

        val doc = db.parse(in);


        val elems = doc.getElementsByTagNameNS(MAVEN_NS, "dependencyManagement");
        if (elems.getLength() != 1)
        {
            throw new RuntimeException("Unexpected platform pom structure. dependencyManagement: " + elems.getLength());
        }

        val depElems = elems.item(0).asInstanceOf[Element].getElementsByTagNameNS(MAVEN_NS, "dependencies");
        if (depElems.getLength() != 1)
        {
            throw new RuntimeException("Unexpected platform pom structure. dependencies: " + depElems.getLength());
        }

        doc.getDocumentElement().appendChild(depElems.item(0));


        val t = TransformerFactory.newInstance().newTransformer();

        val f = File.createTempFile("temp-pom", ".xml");
        f.deleteOnExit();

        t.transform(new DOMSource(doc), new StreamResult(f));

        val f2 = File.createTempFile("dependency-output", ".txt");
        f2.deleteOnExit();

        val cmd = Array(
                "mvn", "-f", f.getPath(), "dependency:resolve", "-DoutputFile=" + f2.getPath(), "-q"
        )

        val p = Runtime.getRuntime().exec(cmd);

        if (p.waitFor() == 0)
        {
            return f2;
        }
        else
        {
            throw new RuntimeException("Maven invocation failed.");
        }
    }

    def parseDeps(in: Reader): Set[String] =
    {
        val result = new HashSet[String]();

        for (s <- IOUtils.readLines(in).asScala)
        {
            if (s.startsWith(" "))
            {
                val i = s.lastIndexOf(':');
                if (i >= 0)
                {
                    result.add(s.substring(0, i).trim());
                }
            }
        }

        return result;
    }

    def getUpgradeTargetFrom(cache: HttpCache, version: String):  Map[String, ArtifactVersion] =
            upgradeTargetFrom(getPlatformDepsFrom(cache, version))

    def upgradeTargetFrom(deps: java.lang.Iterable[String]):  Map[String, ArtifactVersion] =
    {
        val m = new HashMap[String, ArtifactVersion]();

        for (s <- deps.asScala)
        {
            val i = s.lastIndexOf(':');
            val j = s.lastIndexOf(':', i - 1);
            if (i < 0 || j < 0)
            {
                throw new IllegalArgumentException("Bad platform dependency: " + s);
            }

            m.put(s.substring(0, j), new DefaultArtifactVersion(s.substring(i + 1)));
        }

        return m;
    }
}

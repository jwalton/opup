package com.atlassian.labs.opup.task
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.RepositoryBuilder
import java.io.File
import org.eclipse.jgit.api.MergeCommand
import org.eclipse.jgit.lib.CommitBuilder
import scala.collection.JavaConverters._
import org.eclipse.jgit.lib.AnyObjectId
import org.eclipse.jgit.lib.ObjectId
import com.atlassian.labs.opup.VersionChanger
import org.apache.maven.artifact.versioning.DefaultArtifactVersion
import org.apache.commons.io.FileUtils
import org.eclipse.jgit.api.ResetCommand.ResetType


class MergeTask(git: Git, commits: Map[ObjectId, VersionChanger], poms: Iterable[File], upgrade: Iterable[String]) {
  def merge(): Unit = {
    val repo = git.getRepository()

    val head = repo.getRef("HEAD").getObjectId()

    val allParents = upgrade.map(ObjectId.fromString(_)).filter(!_.equals(head)).toSeq

    if (allParents.isEmpty) {
      return
    }

    git.checkout().setName(allParents.head.getName()).call()
    repo.writeMergeHeads(allParents.tail.asJava)

    val vc = new VersionChanger()

//    println("All commits: " + commits)
//    println("All parents: " + allParents)

    allParents.foreach(id => {
      vc.addAll(commits.get(id).get)
    })

    vc.changeAll(poms)

    git.add().addFilepattern(".").setUpdate(true).call()
    val mergeCommit = git.commit().setMessage("OpUp upgrade.\n\n" + vc.toString().replaceAll(", ", "\n")).call()

    git.checkout().setName("master").call()
    git.reset().setRef(mergeCommit.getId().getName()).setMode(ResetType.HARD).call()
  }
}

object MergeTask {
  def loadVersionChangers(f: File): Map[ObjectId, VersionChanger] = {
    val commits = collection.mutable.Map[ObjectId, VersionChanger]()

    for (l <- FileUtils.lineIterator(f).asScala) {
      val k = l.split(',')

      val vc = new VersionChanger()

      val k2 = k(0).split(':')
      if (k2.length == 2) {
        vc.setNewPropertyValue(k2(0), new DefaultArtifactVersion(k2(1)))
      } else if (k2.length == 3) {
        vc.setNewVersion(k2(0) + ':' + k2(1), new DefaultArtifactVersion(k2(2)))
      }

      commits.put(ObjectId.fromString(k(1)), vc)
    }

    return commits.toMap
  }

  def from(workingDirectory: File, commitKeyFile: File, poms: Iterable[File], upgradeRefFile: File): MergeTask = {
    val repo = new RepositoryBuilder().setWorkTree(workingDirectory).build();
    val git = new Git(repo);

    val versionChangers = loadVersionChangers(commitKeyFile)

    val upgradeRefs = FileUtils.lineIterator(upgradeRefFile).asScala.toList

    return new MergeTask(git, versionChangers, poms, upgradeRefs)
  }
}

package com.atlassian.labs.opup

import java.io.InputStream
import java.net.URLEncoder
import java.util.regex.Pattern
import javax.json.{Json, JsonArray, JsonObject, JsonString}
import javax.xml.parsers.DocumentBuilderFactory

import org.apache.http.Header
import org.apache.http.message.BasicHeader
import org.apache.maven.artifact.versioning.{ArtifactVersion, DefaultArtifactVersion}

import scala.util.Try

class ArtifactoryVersionClient(cache: HttpCache, forServer: Boolean, artifactoryUrl: String, apiKey: String) extends PackagingFilteringVersionClient {
  val docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()

  def getVersionsAndPackagingTypes(groupId: String, artifactId: String): List[(ArtifactVersion, String)] = {
    val url = artifactoryUrl + "/api/search/gavc?" +
      "g=" + URLEncoder.encode(groupId, "utf-8") +
      "&a=" + URLEncoder.encode(artifactId, "utf-8")

    val headers: Array[Header] = Array(
      new BasicHeader("X-JFrog-Art-Api", apiKey)
    )
    val in = cache.get(url, headers)

    try {
      getVersionsSearch(in, groupId, artifactId, groupId + ":" + artifactId)
    } finally {
      in.close()
    }
  }

  def getVersionsSearch(jsonStream: InputStream, groupId: String, artifactId: String, coords: String): List[(ArtifactVersion, String)] = {
    val versions = collection.mutable.LinkedHashSet[(ArtifactVersion, String)]()

    val rdr = Json.createReader(jsonStream)
    try {
      val parsedObject = rdr.readObject()
      val versionJsonArrays = parsedObject.get("results").asInstanceOf[JsonArray]
      val length = versionJsonArrays.size()
      for (i <- 0 until length) {
        val uri = versionJsonArrays.get(length - 1 - i).asInstanceOf[JsonObject].get("uri").asInstanceOf[JsonString].getString()
        if (!uri.endsWith("sources.jar") && !uri.endsWith(".pom")) {
          val versionRegex = ".*" + groupId.replaceAll("\\.", "/") + "/" + artifactId + "/(.*)/" + artifactId + ".*"
          val pattern = Pattern.compile(versionRegex)
          val matcher = pattern.matcher(uri)
          if (matcher.find()) {
            val extension = uri.substring(uri.lastIndexOf(".") + 1)
            val version = matcher.group(1)
            val dav = new DefaultArtifactVersion(version)
            if (version.indexOf(".") > -1) {
              val major = version.substring(0, version.indexOf("."))
              if (Try(major.toInt).isSuccess) {
                if (forServer) {
                  if (major.toInt < 1000) {
                    versions += ((dav: ArtifactVersion, extension))
                  }
                } else {
                  versions += ((dav: ArtifactVersion, extension))
                }
              } else {
                // This is to handle special and bad semantic version. For e.g: atlassian-event-2.0.0
                versions += ((dav: ArtifactVersion, extension))
              }
            } else {
              // This is to handle special and bad semantic version. For e.g: r09
              versions += ((dav: ArtifactVersion, extension))
            }
          }
        }
      }
    } finally {
      rdr.close()
    }

    return versions.toList
  }
}

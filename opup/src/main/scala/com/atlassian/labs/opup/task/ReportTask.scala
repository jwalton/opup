package com.atlassian.labs.opup.task

import java.io.File
import org.apache.http.client.CredentialsProvider
import com.atlassian.labs.opup.VersionClient
import com.atlassian.labs.opup.HttpCache
import com.atlassian.labs.opup.UpgradeAdvice

class ReportTask(upgradeStrategy: String, httpCache: HttpCache, advice: UpgradeAdvice, versionClient: VersionClient)
    extends AbstractOpUpTask(upgradeStrategy, httpCache, advice, versionClient, new File("."))
{
}

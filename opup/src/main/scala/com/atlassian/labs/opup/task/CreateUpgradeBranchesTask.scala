package com.atlassian.labs.opup.task

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.util.ArrayList
import java.util.HashMap
import scala.collection.JavaConverters.asScalaBufferConverter
import scala.collection.JavaConverters.asScalaSetConverter
import scala.collection.JavaConverters.mapAsScalaMapConverter
import scala.collection.mutable.ListBuffer
import org.apache.http.client.CredentialsProvider
import org.apache.maven.plugin.logging.Log
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.ObjectId
import org.eclipse.jgit.lib.RepositoryBuilder
import com.atlassian.labs.opup.UpgradeBranchCreator
import com.atlassian.labs.opup.VersionChanger
import com.atlassian.labs.opup.HttpCache
import com.atlassian.labs.opup.UpgradeAdvice
import com.atlassian.labs.opup.VersionClient

class CreateUpgradeBranchesTask(upgradeStrategy: String, httpCache: HttpCache,
    advice: UpgradeAdvice, versionClient: VersionClient, gitWorkingDirectory: File)
    extends AbstractOpUpTask(upgradeStrategy, httpCache, advice, versionClient, gitWorkingDirectory)
{
  var poms: Seq[File] = _

  def generateUpgradeBranches(reportFile: File, log: Log): Map[ObjectId, VersionChanger] = {
      val (poms, currentVersions, currentProperties, upgradeSchedule) = generateReport(reportFile)

      this.poms = poms

        val changerForEverything = new VersionChanger()
        changerForEverything.setKey("all")

        val allChanges = new HashMap[String, Seq[VersionChanger]]();

        val changeKeys = new ArrayList[String]();

        for (e <- upgradeSchedule.getNewVersions().entrySet().asScala)
        {
            val sb = new StringBuilder(e.getKey());

            sb.append(',');
            sb.append(currentVersions.get(e.getKey()).get)

            val branchName = "artifact-" + (e.getKey().replaceAll("[^a-zA-Z0-9]", "_"));

            val cl = new ListBuffer[VersionChanger]();

            for (v <- e.getValue.asScala)
            {
                val changer = new VersionChanger();

                changer.setNewVersion(e.getKey(), v);
                changer.setKey(e.getKey() + ":" + v);
                changerForEverything.setNewVersion(e.getKey(), v);

                cl += changer

                sb.append(',');
                sb.append(v);
            }

            allChanges.put(branchName, cl);
            changeKeys.add(sb.toString());
        }

        for (e <- upgradeSchedule.getNewProperties().entrySet().asScala)
        {
            val sb = new StringBuilder(e.getKey());

            sb.append(',');
            sb.append(currentProperties.get(e.getKey()).get)

            val branchName = "property-" + e.getKey();

            val cl = new ListBuffer[VersionChanger]();

            for (v <- e.getValue.asScala)
            {
                val changer = new VersionChanger();
                changer.setNewPropertyValue(e.getKey(), v);
                changer.setKey(e.getKey() + ":" + v);
                changerForEverything.setNewPropertyValue(e.getKey(), v);
                cl += changer

                sb.append(',');
                sb.append(v);
            }

            allChanges.put(branchName, cl);
            changeKeys.add(sb.toString());
        }

        allChanges.put("all", Seq(changerForEverything));

        /* Create the branches */

        val repo = new RepositoryBuilder().setWorkTree(gitWorkingDirectory).build();

        val git = new Git(repo);

        val upgradeBranchCreator = new UpgradeBranchCreator(git)

        upgradeBranchCreator.checkout("master")

        val logFunc = (s: String) => log.info(s)

        upgradeBranchCreator.deleteAllOpupBranches(logFunc)

        val commits = upgradeBranchCreator.createBranches(
            allChanges.asScala.toMap,
            poms,
            logFunc)

        upgradeBranchCreator.checkout("master")

        val commitKeys = for ((cmtId, changer) <- commits) yield changer.getKey() + "," + cmtId.getName()

        /* Write out the state */
        writeLines("target/opup/change.keys", changeKeys.asScala);
        writeLines("target/opup/commit.keys", commitKeys);

        return commits
  }

    def writeLines(filename: String, lines: Iterable[String]) = {
        val bw = new BufferedWriter(new FileWriter(filename));
        try
        {
          for (l <- lines) {
                bw.write(l)
                bw.newLine()
           }
        }
        finally
        {
            bw.close();
        }
    }
}

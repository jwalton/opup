package com.atlassian.labs.opup

import java.io.{IOException, InputStream}

import org.apache.http.Header

trait HttpCache {
    @throws(classOf[IOException])
    def get(url: String): InputStream

    @throws(classOf[IOException])
    def get(url: String, headers: Array[Header]): InputStream
}

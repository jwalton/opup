package com.atlassian.labs.opup.task

import java.io.{File, FileInputStream, FileWriter, PrintWriter}
import java.net.Authenticator

import scala.collection.JavaConverters._
import scala.collection.JavaConverters.collectionAsScalaIterableConverter
import org.apache.maven.artifact.versioning.ArtifactVersion
import org.apache.maven.model.io.xpp3.MavenXpp3Reader
import com.atlassian.labs.opup.HttpCache
import com.atlassian.labs.opup.NexusVersionClient
import com.atlassian.labs.opup.PomScanner
import com.atlassian.labs.opup.Report
import com.atlassian.labs.opup.UpgradeAdvice
import com.atlassian.labs.opup.UpgradeSchedule
import com.atlassian.labs.opup.UpgradeStrategy
import com.atlassian.labs.opup.VersionClient
import com.atlassian.labs.opup.VerySimpleCache
import org.apache.http.client.CredentialsProvider


class AbstractOpUpTask(val upgradeStrategy: String, httpCache: HttpCache, advice: UpgradeAdvice,
    versionClient: VersionClient, val baseDirectory: File) {
    val mainPom = new MavenXpp3Reader().read(new FileInputStream(new File(baseDirectory, "pom.xml")))

    def generateReport(htmlFile: File): (Seq[File], Map[String, ArtifactVersion], Map[String, ArtifactVersion], UpgradeSchedule)
        = generateReport(htmlFile, None, None)

    def generateReport(htmlFile: File, opUpUpgradeReport: File, junitReportFile: File): (Seq[File], Map[String, ArtifactVersion], Map[String, ArtifactVersion], UpgradeSchedule)
        = generateReport(htmlFile, Some(opUpUpgradeReport), Some(junitReportFile))

    def mkParentsOf(f: File) = {
        val dir = f.getParentFile()
        if (!dir.exists)
        {
          dir.mkdirs()
        }
        dir
    }

    def generateReport(htmlFile: File, opUpUpgradeReport: Option[File], junitReportFile: Option[File]): (Seq[File], Map[String, ArtifactVersion], Map[String, ArtifactVersion], UpgradeSchedule) = {
        val targetDir = mkParentsOf(htmlFile)

        val report = new Report(mainPom.getGroupId(), mainPom.getArtifactId(), mainPom.getVersion())
        report.copyStylesheet(targetDir)

        val ps = new PomScanner();

        val poms = PomScanner.pomFiles(baseDirectory);

        ps.process(poms.asScala)

        ps.findCurrent(report);
        val currentVersions = ps.getCurrentVersions();
        val currentProperties = ps.getCurrentProperties();

        report.setUpgradeStrategy(upgradeStrategy);
        val targetVersions = new UpgradeStrategy(httpCache).getUpgradeTarget(upgradeStrategy, ps.getCurrentVersions().asScala.toMap)

        var upgradeSchedule = UpgradeSchedule.from(currentVersions, ps, versionClient, advice.obsoleteAdvice, report, targetVersions);

        upgradeSchedule = upgradeSchedule.withUpgrades();

        report.schedule(currentVersions, ps.getCurrentProperties(), upgradeSchedule);

        val w = new FileWriter(htmlFile);
        try
        {
            report.writeReport(w);
        }
        finally
        {
            w.close();
        }

        for (f <- junitReportFile)
        {
            mkParentsOf(f)
            val w2 = new FileWriter(f)
	        try
	        {
	            report.writeJunitReport(w2)
	        }
	        finally
	        {
	            w2.close()
	        }
        }

        for (f <- opUpUpgradeReport)
        {
          val w2 = new PrintWriter(f)
          try
          {
            report.writeOpUpUpgradeReport(w2)
          }
          finally
          {
            w2.close()
          }
        }

        return (poms.asScala.toSeq, currentVersions.asScala.toMap, currentProperties.asScala.toMap, upgradeSchedule)
    }
}

package com.atlassian.labs.opup.task

import java.io.File
import java.io.FileInputStream
import java.io.FileWriter
import java.util.ArrayList
import java.util.Arrays
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.Properties
import java.util.TreeMap

import scala.collection.JavaConverters.asScalaBufferConverter

import org.apache.commons.io.FileUtils
import org.apache.maven.artifact.versioning.ArtifactVersion
import org.apache.maven.artifact.versioning.DefaultArtifactVersion
import org.apache.maven.model.io.xpp3.MavenXpp3Reader
import org.apache.velocity.VelocityContext
import org.apache.velocity.app.VelocityEngine

class ShowResultsTask(opupDirectory: File, passFailFile: File, val relativeLogDirectory: String) {
  val pomFile = new File("pom.xml")

    def generateResults(resultsFile: File) : Unit = {
        val commits = new HashMap[String, String]();

        for (c <- FileUtils.readLines(new File(opupDirectory, "commit.keys")).asScala)
        {
            val sa = c.split(",", 2);

            commits.put(sa(0), sa(1));
        }

        val passes = new HashMap[String, Boolean]();

        for (pf <- FileUtils.readLines(passFailFile).asScala)
        {
            val sa = pf.split(",");

            passes.put(sa(0), sa(1).equals("good"));
        }

        val allResults = new ArrayList[ItemResults]();

        for (change <- FileUtils.readLines(new File(opupDirectory, "change.keys")).asScala)
        {
            val sa = change.split(",");

            val buildResults = new TreeMap[ArtifactVersion, BuildResult]();

            for (vs <- sa.tail)
            {
                val k = sa(0) + ':' + vs

                var result = new BuildResult(false, false, null);

                val commit = commits.get(k);
                if (commit != null)
                {
                    val pf = Option(passes.get(commit));
                    pf.foreach(p =>
                    {
                      result = new BuildResult(true, p, relativeLogDirectory + "/build-log-" + commit + ".txt")
                    })
                }

                buildResults.put(new DefaultArtifactVersion(vs), result);
            }

            implicitise(buildResults, Arrays.asList(sa: _*).subList(1, sa.length));

            val results = new ItemResults(sa(0), buildResults)
            allResults.add(results);
        }

        val ve = new VelocityEngine();

        val props = new Properties();
        props.setProperty("resource.loader", "class");
        props.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        props.setProperty("eventhandler.referenceinsertion.class", "org.apache.velocity.app.event.implement.EscapeHtmlReference");

        ve.init(props);

        val ctxt = new VelocityContext();

        val mainPom = new MavenXpp3Reader().read(new FileInputStream(pomFile))
        ctxt.put("groupId", mainPom.getGroupId());
        ctxt.put("artifactId", mainPom.getArtifactId());
        ctxt.put("version", mainPom.getVersion());

        ctxt.put("results", allResults);

        val template = ve.getTemplate("/com/atlassian/labs/opup/results.vm");

        val w = new FileWriter(resultsFile)
        try
        {
            template.merge(ctxt, w);
            w.flush();
        }
        finally
        {
            w.close();
        }
    }

    def implicitise(buildResults: Map[ArtifactVersion, BuildResult], subList: List[String]) =
    {
        /* Current version passes implicitly */
        val current = new DefaultArtifactVersion(subList.get(0));
        if (!buildResults.get(current).ran)
        {
            val br = new BuildResult(false, true, null)
            buildResults.put(current, br);
        }

        val implied = new HashMap[ArtifactVersion, BuildResult]();

        var haveRealResults = false;
        var noFailures = true;
        for (s <- subList.asScala)
        {
            val v = new DefaultArtifactVersion(s);

            val real = buildResults.get(v);
            if (real.ran)
            {
                haveRealResults = true;
                noFailures &= real.passed;
            }
            else
            {
                val ibr = new BuildResult(false, noFailures, null);
                implied.put(v, ibr);
            }
        }

        if (haveRealResults)
        {
            buildResults.putAll(implied);
        }
    }
}

case class BuildResult(val ran: Boolean, val passed: Boolean, logfile: String)
{
    def getRan() = ran

    def getPassed() = passed

    def getLogfile() = logfile
}

class ItemResults(key: String, results: Map[ArtifactVersion, BuildResult])
{
    def getKey() = key

    def getResults() = results
}

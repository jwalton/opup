package com.atlassian.labs.opup

import org.apache.maven.artifact.versioning.ArtifactVersion
import org.apache.maven.artifact.versioning.DefaultArtifactVersion

object JUnitReport {

  def asTestcase(artifact: String, currentVersion: ArtifactVersion, availableVersion: ArtifactVersion) = {
    val artifactId = artifact.replaceAll("[^a-zA-Z0-9]", "_")

    val testName = "use_" + artifactId + "_Version_" + availableVersion

    <testcase classname={artifactId} name={testName}>{if (currentVersion.compareTo(availableVersion) < 0) {
        <failure type="NotUpToDate">Artifact {artifact} version {availableVersion} is available, but this project uses {currentVersion}.</failure>
      }
    }</testcase>
  }

  def toJunitReport(items: Traversable[(String, ArtifactVersion, ArtifactVersion)]) =
    <testsuite package="com.atlassian.labs.opup" name="OpUp JUnit Report">
    {items.map(x => asTestcase(x._1, x._2, x._3))}
</testsuite>
}

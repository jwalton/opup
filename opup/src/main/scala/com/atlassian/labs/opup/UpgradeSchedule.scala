package com.atlassian.labs.opup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.xml.sax.SAXException;

import scala.collection.JavaConverters._

class UpgradeSchedule(val newVersions: Map[String, List[ArtifactVersion]], val newProperties: Map[String, List[ArtifactVersion]])
{
    def withUpgrades(): UpgradeSchedule =
    {
        val versions = UpgradeSchedule.notEmptyLists(newVersions);
        val properties = UpgradeSchedule.notEmptyLists(newProperties);

        return new UpgradeSchedule(versions, properties);
    }

    def getNewVersions(): Map[String, List[ArtifactVersion]] = new TreeMap[String, List[ArtifactVersion]](newVersions);

    def getNewProperties(): Map[String, List[ArtifactVersion]] = new TreeMap[String, List[ArtifactVersion]](newProperties);
}

object UpgradeSchedule
{
    // XXX Should take more general data structures
    def from(currentVersions: Map[String, ArtifactVersion], ps: PomScanner,
            vc: VersionClient, obsolete: ObsoleteAdvice, report: ReportTarget,
            upgradeTarget: (String, ArtifactVersion) => Boolean): UpgradeSchedule =
    {
        val newVersions = new HashMap[String, List[ArtifactVersion]]();
        val newProperties = new HashMap[String, List[ArtifactVersion]]();

        for (e <- currentVersions.entrySet.asScala)
        {
            val coords = e.getKey();
            val version = e.getValue();

//            System.out.println(coords + " " + version);

            val sa = coords.split(":");
            if (sa.length < 2 || sa.length > 3)
            {
                throw new RuntimeException("Bad coord: " + coords);
            }

            val packaging =
            if (sa.length == 3)
            {
                sa(2);
            }
            else
            {
                "jar";
            }

            for (advice <- obsolete.getAdvice(sa(0) + ':' + sa(1)))
            {
                report.obsolete(sa(0) + ':' + sa(1), advice)
            }

            val knownVersions = vc.getVersions(sa(0), sa(1), packaging, report);

            val aims = aimedFor(sa(0) + ':' + sa(1), knownVersions, upgradeTarget);

            var moreRecent = VersionClientUtil.since(aims, version).asJava

            val prop = ps.getPropertyForArtifactVersion(coords);
            if (prop != null)
            {
                if (newProperties.containsKey(prop))
                {
                    val existingNewVersions = newProperties.get(prop);

                    if (!moreRecent.equals(existingNewVersions))
                    {
                        moreRecent = new ArrayList[ArtifactVersion](moreRecent);
                        moreRecent.retainAll(existingNewVersions);
                    }
                }

                newProperties.put(prop, moreRecent);
            }
            else
            {
                newVersions.put(coords, moreRecent);
            }
        }

        return new UpgradeSchedule(newVersions, newProperties);
    }

    def aimedFor(artifact: String, knownVersions: scala.List[ArtifactVersion],
            isUpgradeTarget: ((String, ArtifactVersion) => Boolean)): scala.List[ArtifactVersion] =
    {
        return knownVersions.filter(isUpgradeTarget(artifact, _))
    }

    def notEmptyLists(m: Map[String, List[ArtifactVersion]]): Map[String, List[ArtifactVersion]] =
    {
        (for ((k, v) <- m.asScala if !v.isEmpty()) yield (k, v)).toMap.asJava
    }
}

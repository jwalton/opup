package com.atlassian.labs.opup

import java.io.FileNotFoundException
import java.io.InputStream
import java.io.InputStreamReader
import java.io.Reader

class UpgradeAdvice(val obsoleteAdvice: ObsoleteAdvice, val versionOverrides: VersionOverrides) {
  def requireFinal(require: Boolean): UpgradeAdvice = {
    return new UpgradeAdvice(this.obsoleteAdvice, versionOverrides.requireFinal(require))
  }
}

object UpgradeAdvice {
  def getReaderOrFail(name: String): Reader = {
    val in = getClass().getResourceAsStream(name)
    if (in == null)
    {
      throw new FileNotFoundException(name)
    }
    return new InputStreamReader(in, "utf-8")
  }

  def fromResources(): UpgradeAdvice = {
    val oa = ObsoleteAdvice.load(getReaderOrFail("obsolete-artifacts.txt"))

    val vo = new VersionOverrides().load(getReaderOrFail("bad-artifacts.txt"))

    return new UpgradeAdvice(oa, vo)
  }
}

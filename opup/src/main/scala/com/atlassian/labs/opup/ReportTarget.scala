package com.atlassian.labs.opup;

import java.util.Collection

import org.apache.maven.artifact.versioning.ArtifactVersion

trait ReportTarget
{
    def multipleVersions(artifact: String, versions: Collection[ArtifactVersion])

    def multipleValues(propname: String, values: Collection[ArtifactVersion])

    def undefinedProperty(artifact: String, propname: String)

    def packagingChange(coords: String, latestVersionWithDifferentPackaging: ArtifactVersion, differentPackaging: String,
            requiredPackaging: String, artifactVersion: ArtifactVersion)

    def obsolete(artifact: String, advice: String)
    
    def implementationLimitation(note: String)
}

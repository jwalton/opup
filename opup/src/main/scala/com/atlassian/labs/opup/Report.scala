package com.atlassian.labs.opup;

import java.io.{File, FileNotFoundException, PrintWriter, Writer}
import java.lang.Override
import java.util.Map
import java.util.ArrayList
import java.util.Collection
import java.util.Collections
import java.util.HashMap
import java.util.Properties

import scala.collection.JavaConverters._
import org.apache.commons.io.FileUtils
import org.apache.maven.artifact.versioning.ArtifactVersion
import org.apache.velocity.app.VelocityEngine
import org.apache.velocity.VelocityContext

import scala.collection.mutable.Buffer
import scala.collection.mutable.ListBuffer

class Report(groupId: String, artifactId: String, version: String) extends ReportTarget {
    private val thingVersionWarnings = new ArrayList[MultipleThingVersionWarning]();
    private val miscWarnings = new ArrayList[String]();

    private var currentVersions: Map[String, ArtifactVersion] = _;
    private var schedule: UpgradeSchedule = _;

    private var strategy: String = _;

    def setUpgradeStrategy(name: String)
    {
        this.strategy = name;
    }

    def writeReport(w: Writer) =
    {
        val ve = new VelocityEngine();

        val props = new Properties();
        props.setProperty("resource.loader", "class");
        props.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        props.setProperty("eventhandler.referenceinsertion.class", "org.apache.velocity.app.event.implement.EscapeHtmlReference");

        ve.init(props);

        val ctxt = new VelocityContext();
        ctxt.put("groupId", groupId);
        ctxt.put("artifactId", artifactId);
        ctxt.put("version", version);
        ctxt.put("upgradeStrategy", strategy);
        ctxt.put("warnings", thingVersionWarnings);
        ctxt.put("miscWarnings", miscWarnings);

        ctxt.put("currentVersions", currentVersions);
        ctxt.put("upgradeSchedule", schedule);

        val template = ve.getTemplate("/com/atlassian/labs/opup/report.vm");

        template.merge(ctxt, w);
        w.flush();
    }

    def copyStylesheet(base: File) =
    {
        val in = getClass().getResourceAsStream("style.css");
        if (in == null)
        {
            throw new FileNotFoundException("Resource style.css not found");
        }

        FileUtils.copyInputStreamToFile(in, new File(base, "style.css"));
    }

    def writeJunitReport(w: Writer)
    {
        val items = new ListBuffer[(String, ArtifactVersion, ArtifactVersion)]

        for ((k, v) <- currentVersions.asScala)
        {
            val x = List(schedule.getNewVersions().asScala.get(k).map(_.asScala),
                    schedule.getNewProperties().asScala.get(k).map(_.asScala)).flatten.flatten

            val i = (k, v, if (x.isEmpty) v else x.max)

            items += i
        }

        w.write(JUnitReport.toJunitReport(items.toList).toString)
    }

    def writeOpUpUpgradeReport(w: PrintWriter): Unit =
    {
        val items = new ListBuffer[(String, ArtifactVersion, ArtifactVersion)]

        for ((k, currentVersion) <- currentVersions.asScala)
        {
            val x = List(schedule.getNewVersions().asScala.get(k).map(_.asScala),
                schedule.getNewProperties().asScala.get(k).map(_.asScala)).flatten.flatten

            val availableVersion = if (x.isEmpty) currentVersion else x.max
            if (currentVersion.compareTo(availableVersion) < 0)
            {
                w.println(s"$k,$currentVersion,$availableVersion")
            }
        }
    }

    @Override
    def multipleVersions(artifact: String, versions: Collection[ArtifactVersion])
    {
        addWarning(new MultipleThingVersionWarning("artifact", artifact, versions.asScala));
    }

    @Override
    def multipleValues(propname: String, values: Collection[ArtifactVersion])
    {
        addWarning(new MultipleThingVersionWarning("property", propname, values.asScala));
    }

    @Override
    def obsolete(artifact: String, advice: String)
    {
        miscWarnings.add(artifact + " is obsolete. " + advice);
    }

    @Override
    def undefinedProperty(artifact: String, propname: String)
    {
        miscWarnings.add("Undefined property " + propname + " used in as version for " + artifact)
    }

    def addWarning(w: MultipleThingVersionWarning)
    {
        thingVersionWarnings.add(w);
        println(w);
    }

    @Override
    def packagingChange(coords: String, latestVersionWithDifferentPackaging: ArtifactVersion,
            differentPackaging: String, requiredPackaging: String, artifactVersion: ArtifactVersion)
    {
        val reportPackaging = if (differentPackaging.isEmpty) "of undefined packaging" else "a " + differentPackaging

        val diag = "Latest " + coords + " (" +
                latestVersionWithDifferentPackaging + ") is " + reportPackaging + ", not a " + requiredPackaging +
                ". Sticking with " + artifactVersion + ".";

        miscWarnings.add(diag);
    }

    def implementationLimitation(note: String)
    {
        miscWarnings.add(note)
    }

    // This is written like a bean so Velocity can use it
    class MultipleThingVersionWarning(thingType: String, thingName: String, versions: Iterable[ArtifactVersion])
    {
        def getType(): String =
        {
            return thingType;
        }

        def getName(): String =
        {
            return thingName;
        }

        def getVersions(): Collection[ArtifactVersion] =
        {
          return versions.toSeq.sorted.asJava
        }

        override def toString(): String =
        {
            return "Multiple versions for " + thingType + " '" + thingName + "': " + versions;
        }
    }

    def schedule(currentVersions: Map[String, ArtifactVersion], currentProperties: Map[String, ArtifactVersion], upgradeSchedule: UpgradeSchedule)
    {
        val current = new HashMap[String, ArtifactVersion]();
        current.putAll(currentVersions);
        current.putAll(currentProperties);
        this.currentVersions = current ;
        this.schedule = upgradeSchedule;
    }
}

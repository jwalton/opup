package com.atlassian.labs.opup

import java.io.Reader

import scala.collection.JavaConverters.collectionAsScalaIterableConverter

import org.apache.commons.io.IOUtils

object Utils {
  /** Read lines from a configuration file, dropping blank lines and comments. */
  def lines(r: Reader) = try {
    IOUtils.readLines(r).asScala.
      map(_.trim).
      filter(s => !s.isEmpty && !s.startsWith("#"))
  } finally {
    r.close
  }
}

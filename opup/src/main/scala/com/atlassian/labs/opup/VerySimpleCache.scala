package com.atlassian.labs.opup

import java.io.{File, FileNotFoundException, FileInputStream, FileOutputStream, IOException, InputStream}

import org.apache.commons.io.IOUtils
import org.apache.http.client.CredentialsProvider
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.impl.client.{CloseableHttpClient, HttpClients}
import org.apache.http.{Header, HttpStatus}

class VerySimpleCache(cacheDir: File, credentialsProvider: CredentialsProvider) extends HttpCache
{
    val httpClient: CloseableHttpClient = HttpClients.createDefault()
    val context = HttpClientContext.create()

    if (credentialsProvider != null)
    {
        context.setCredentialsProvider(credentialsProvider)
    }

    if (!cacheDir.exists())
    {
        cacheDir.mkdirs();
    }

    if (!cacheDir.isDirectory())
    {
        throw new IllegalArgumentException("Not a directory: " + cacheDir);
    }

    def get(url: String): InputStream =
    {
        return get(url, null)
    }

    def get(url: String, headers: Array[Header]): InputStream =
    {
        val fn = url.replaceAll("[^a-zA-Z0-9]", "_")

        val f = new File(cacheDir, fn)

        if (!f.exists())
        {
            val get = new HttpGet(url)
            if (headers != null)
            {
                get.setHeaders(headers)
            }

            val response = httpClient.execute(get, context)

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_FOUND)
            {
                throw new FileNotFoundException("Failed to retrieve " + url);
            }

            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK)
            {
                throw new IOException("Failed to retrieve " + url + ": " + response.getStatusLine());
            }

            val in = response.getEntity().getContent()

            try
            {
                val out = new FileOutputStream(f);
                try
                {
                    IOUtils.copy(in, out);
                }
                finally
                {
                    out.close();
                }
            }
            finally
            {
                in.close();
            }

        }

        return new FileInputStream(f);
    }
}

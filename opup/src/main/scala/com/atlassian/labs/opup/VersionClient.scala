package com.atlassian.labs.opup

import org.apache.maven.artifact.versioning.ArtifactVersion

trait VersionClient {
    def getVersions(groupId: String, artifactId: String, requiredPackaging: String, report: ReportTarget): List[ArtifactVersion]
}

trait PackagingFilteringVersionClient extends VersionClient {
  // Allow different packaging types that still produces jars to be used as jars
  private def normalise(packaging: String) = packaging match {
    case "jar" | "bundle" | "atlassian-plugin" => "jar"
    case p => p
  }

    def getVersions(groupId: String, artifactId: String, requiredPackaging: String, report: ReportTarget): List[ArtifactVersion] = {
      val coords = groupId + ":" + artifactId

      val versions = collection.mutable.Set[ArtifactVersion]();

      var latestVersionWithDifferentPackaging: ArtifactVersion = null
      var differentPackaging: String = null

      for ((dav, packaging) <- getVersionsAndPackagingTypes(groupId, artifactId))
        if (normalise(packaging) == normalise(requiredPackaging))
        {
            versions.add(dav);
        }
        else
        {
            latestVersionWithDifferentPackaging = dav;
            differentPackaging = packaging;
        }

      val vl = versions.toList.sorted

      if (!vl.isEmpty && latestVersionWithDifferentPackaging != null && (vl.last.compareTo(latestVersionWithDifferentPackaging) < 0) && report != null)
      {
        report.packagingChange(coords, latestVersionWithDifferentPackaging, differentPackaging, requiredPackaging, vl.last);
      }

      return vl
    }

    def getVersionsAndPackagingTypes(groupId: String, artifactId: String): List[(ArtifactVersion, String)]
}

object VersionClientUtil
{
    def since(versions: List[ArtifactVersion], current: ArtifactVersion): List[ArtifactVersion] =
    {
      return versions.filter(_.compareTo(current) > 0).toList
    }

    def filtered(vc: VersionClient, versionOverrides: VersionOverrides): VersionClient = {
      new VersionClient {
        def getVersions(groupId: String, artifactId: String, requiredPackaging: String, report: ReportTarget) =
          vc.getVersions(groupId, artifactId, requiredPackaging, report).filter(avs => versionOverrides.isValid(groupId + ":" + artifactId, avs) && !"SNAPSHOT".equals(avs.getQualifier()))
      }
    }
}

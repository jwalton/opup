package com.atlassian.labs.opup

import scala.collection.JavaConverters.asScalaBufferConverter

import org.apache.maven.artifact.Artifact
import org.apache.maven.artifact.DefaultArtifact
import org.apache.maven.artifact.handler.ArtifactHandler
import org.apache.maven.artifact.handler.DefaultArtifactHandler
import org.apache.maven.artifact.repository.ArtifactRepository
import org.apache.maven.artifact.repository.metadata.ArtifactRepositoryMetadata
import org.apache.maven.artifact.repository.metadata.RepositoryMetadata
import org.apache.maven.artifact.repository.metadata.RepositoryMetadataManager
import org.apache.maven.artifact.versioning.ArtifactVersion
import org.apache.maven.artifact.versioning.DefaultArtifactVersion
import org.apache.maven.artifact.versioning.VersionRange
import org.apache.maven.repository.legacy.metadata.DefaultMetadataResolutionRequest
import org.apache.maven.repository.legacy.metadata.MetadataResolutionRequest


class MavenArtifactVersionClient(
    metadataManager: RepositoryMetadataManager,
    localRepository: ArtifactRepository,
    remoteRepositories: java.util.List[ArtifactRepository]) extends VersionClient {

    val AllVersions = VersionRange.createFromVersionSpec("[0,)")

    def getVersions(groupId: String, artifactId: String, packaging: String, report: ReportTarget): List[ArtifactVersion] = {
      val ah: ArtifactHandler = new DefaultArtifactHandler()
      val artifact: Artifact = new DefaultArtifact(groupId, artifactId, AllVersions, null, packaging, null, ah)

      val metadata: RepositoryMetadata = new ArtifactRepositoryMetadata(artifact);

      val request: MetadataResolutionRequest = new DefaultMetadataResolutionRequest()
      request.setArtifact(artifact)
      request.setLocalRepository(localRepository);
      request.setRemoteRepositories(remoteRepositories);

      metadataManager.resolve(metadata, request);

      Option(metadata.getMetadata().getVersioning()).fold(List[ArtifactVersion]()) {
        _.getVersions().asScala.toList.map(new DefaultArtifactVersion(_): ArtifactVersion)
      }
  }
}

package com.atlassian.labs.opup

import org.apache.maven.artifact.versioning.ArtifactVersion

import scala.collection.JavaConverters._

class UpgradeStrategy(cache: HttpCache) {
  private val platformStrategy = """platform-(.+)""".r
  private val groupIdStrategy = """groupIdRoot-(.+)""".r

  private def singleUpgradeTarget(strategy: String, current: Map[String, ArtifactVersion]): ((String, ArtifactVersion) => Boolean) = strategy match {
    case "latest" => UpgradeStrategy.LATEST
    case "incremental" => UpgradeStrategy.incremental(current)
    case "minor" => UpgradeStrategy.minor(current)
    case platformStrategy(version) => UpgradeStrategy.forVersions(platform.GetPlatformArtifacts.getUpgradeTargetFrom(cache, version).asScala.toMap)
    case groupIdStrategy(root) => UpgradeStrategy.withGroupIdRoot(root)
    case _ => throw new IllegalArgumentException()
  }

  @throws(classOf[java.io.IOException])
  def getUpgradeTarget(strategy: String, current: Map[String, ArtifactVersion]): ((String, ArtifactVersion) => Boolean) = {
    strategy.split(',').toList.map(singleUpgradeTarget(_, current)) match {
      case target :: Nil => target
      case targets => (a: String, v: ArtifactVersion) => !targets.exists(!_(a, v))
    }
  }
}

object UpgradeStrategy {
  val LATEST = (artifact: String, version: ArtifactVersion) => true

  def forVersions(m: Map[String, ArtifactVersion]): (String, ArtifactVersion) => Boolean = {
    (a, v) => {
      m.get(a) match {
        case Some(x) => v.compareTo(x) <= 0
        case None => false
      }
    }
  }

  def incremental(current: Map[String, ArtifactVersion]): (String, ArtifactVersion) => Boolean = {
    (a, v) => {
      current.get(a) match {
        case Some(x) => v.getMajorVersion() == x.getMajorVersion() && v.getMinorVersion() == x.getMinorVersion() && v.getIncrementalVersion() >= x.getIncrementalVersion()
        case None => false
      }
    }
  }

  def minor(current: Map[String, ArtifactVersion]): (String, ArtifactVersion) => Boolean = {
    (a, v) => {
      current.get(a) match {
        case Some(x) => v.getMajorVersion() == x.getMajorVersion() && v.getMinorVersion() >= x.getMinorVersion() && (
            v.getIncrementalVersion() >= x.getIncrementalVersion() || v.getMinorVersion() > x.getMinorVersion())
        case None => false
      }
    }
  }

  private val groupIdPattern = """(.*?):.*""".r

  def withGroupIdRoot(root: String): (String, ArtifactVersion) => Boolean = {
    (a, v) => a match {
      case groupIdPattern(groupId) => groupId == root || groupId.startsWith(root + ".")
      case _ => false
    }
  }
}

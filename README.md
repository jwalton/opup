# OpUp

OpUp shows you the available dependency upgrades for your Maven project
as a report. It will then try to make all those upgrades and let
you know about any that break your tests. By bisecting the upgrades,
it will let you know the version that causes the problem and show you
why.

## Maven

* mvn install

### Generating a report:

    mvn -o com.atlassian.maven.plugins:opup-maven-plugin:report -Dnexus.url=https://maven.atlassian.com -Dopup.cache="$HOME/.opup/cache"

* nexus.url - https://maven.atlassian.com or https://m2proxy.atlassian.com
* nexus.serverid - atlassian-private (use passwords from this server in ~/.m2/settings.xml)
* strategy - latest, platform-2.13.0 (or other version)
* requireFinal - only consider final releases

### Running an upgrade:

    mvn -o com.atlassian.maven.plugins:opup-maven-plugin:upgrade -Dnexus.url=https://maven.atlassian.com -Dopup.cache="$HOME/.opup/cache"

* buildCommand - "mvn clean test"

### Strategies

* latest - use the latest version of everything
* platform-x.yy - use everything up to that release of the Atlassian Plugin SDK
* incremental - leave the major and minor version numbers as they are
* minor - leave the major version number as it is
* groupIdRoot-XXX - only upgrade artifacts with a groupId that begins with XXX
* Strategies separated by commas will only upgrade if all strategies approve
   e.g., -Dstrategy=groupIdRoot-org.apache,minor to take minor upgrades of
   Apache artifacts

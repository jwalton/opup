package com.atlassian.maven.plugins.opup.mojo;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class UpgradeMojoTest
{
    @Test
    public void checkScriptsAreAvailable()
    {
        String[] scripts = {
            "build-and-log.sh",
            "find-success.sh"
        };

        for (String s : scripts)
        {
            assertNotNull(s, UpgradeMojo.class.getResourceAsStream(s));
        }
    }
}

package com.atlassian.maven.plugins.opup.mojo;

import java.io.File;

import com.atlassian.labs.opup.task.ReportTask;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "report", aggregator = true)
public class ReportMojo extends AbstractOpUpMojo
{
    @Parameter(defaultValue = "${project.build.directory}/surefire-reports/OPUP-upgrade-report.xml")
    public File junitResultsFile;

    @Parameter(defaultValue = "${project.build.directory}/opup-report.txt")
    public File opUpUpgradeReportFile;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        ReportTask task = new ReportTask(strategy, newHttpCache(), getUpgradeAdvice(), getVersionClient());
        task.generateReport(reportFile, opUpUpgradeReportFile, junitResultsFile);

        getLog().info("OpUp report: " + "file://" + reportFile);
    }
}

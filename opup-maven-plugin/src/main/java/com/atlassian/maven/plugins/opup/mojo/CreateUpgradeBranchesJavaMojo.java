package com.atlassian.maven.plugins.opup.mojo;

import java.io.File;

import com.atlassian.labs.opup.task.CreateUpgradeBranchesTask;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "create-upgrade-branches", aggregator = true)
public class CreateUpgradeBranchesJavaMojo extends AbstractOpUpMojo
{
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        CreateUpgradeBranchesTask task = new CreateUpgradeBranchesTask(strategy, newHttpCache(), getUpgradeAdvice(), getVersionClient(), new File("."));

        task.generateUpgradeBranches(reportFile, getLog());

        getLog().info("OpUp report: " + "file://" + reportFile);
    }
}

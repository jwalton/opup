package com.atlassian.maven.plugins.opup.mojo;

import java.io.File;

import com.atlassian.labs.opup.task.ShowResultsTask;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "show-results", aggregator = true)
public class ShowResultsMojo extends AbstractMojo
{
    @Parameter(defaultValue = "${project.build.directory}/opup")
    public File opupDirectory;

    @Parameter(defaultValue = "${project.build.directory}/opup/opup-log/build-pass-fail")
    public File passFailFile;

    @Parameter(defaultValue = "opup/opup-log")
    public String relativeLogDirectory;

    @Parameter(defaultValue = "${project.build.directory}/opup-results.html")
    public File resultsFile;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        ShowResultsTask task = new ShowResultsTask(opupDirectory, passFailFile, relativeLogDirectory);

        task.generateResults(resultsFile);

        getLog().info("OpUp upgrade results: " + "file://" + resultsFile);
    }
}

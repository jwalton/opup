package com.atlassian.maven.plugins.opup.mojo;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import com.atlassian.labs.opup.ArtifactoryVersionClient;
import com.atlassian.labs.opup.HttpAuthentication;
import com.atlassian.labs.opup.HttpCache;
import com.atlassian.labs.opup.MavenArtifactVersionClient;
import com.atlassian.labs.opup.NexusVersionClient;
import com.atlassian.labs.opup.UpgradeAdvice;
import com.atlassian.labs.opup.VersionClient;
import com.atlassian.labs.opup.VersionClientUtil;
import com.atlassian.labs.opup.VerySimpleCache;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.repository.metadata.RepositoryMetadataManager;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.settings.Server;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcher;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcherException;

public abstract class AbstractOpUpMojo extends AbstractMojo
{
    @Parameter(property = "strategy", defaultValue = "latest")
    public String strategy;

    @Parameter(defaultValue = "${project.build.directory}/opup-report.html")
    public File reportFile;

    @Parameter(property = "opup.cache", defaultValue="${project.build.directory}/opup/cache")
    public File opupCache;

    @Parameter(property = "nexus.url")
    public String nexusUrl;

    @Parameter(property = "nexus.serverid")
    public String nexusServerId;

    /**
     * Follow an Atlassian-specific convention for distinguishing Cloud and
     * Server artifact versions when querying Artifactory.
     */
    @Parameter(property = "forServer", defaultValue = "true")
    public boolean forServer;

    @Parameter(property = "artifactory.url")
    public String artifactoryUrl;

    @Parameter(property = "artifactory.apiKey")
    public String artifactoryApiKey;

    @Parameter(property = "requireFinal", defaultValue = "true")
    public boolean requireFinal;

    @Component
    public RepositoryMetadataManager repositoryMetadataManager;

    @Parameter(property = "localRepository", required = true)
    public ArtifactRepository localRepository;

    @Parameter(property = "project.remoteArtifactRepositories", required = true)
    public List<ArtifactRepository> remoteRepositories;

    /* Components to get hold of repository passwords */

    /**
     * The Maven Session Object
     */
    @Parameter(property = "session", required = true, readonly = true)
    private MavenSession session;

    @Component(hint = "mng-4384")
    SecDispatcher securityDispatcher;

    CredentialsProvider getNexusCredentials() throws MojoExecutionException
    {
        if (nexusServerId != null)
        {
            Server server = session.getSettings().getServer(nexusServerId);
            if (server == null)
            {
                throw new MojoExecutionException("Unknown server ID: " + nexusServerId);
            }

            String password;

            try
            {
                password = securityDispatcher.decrypt(server.getPassword());
            }
            catch (SecDispatcherException sde)
            {
                throw new MojoExecutionException("Failed to decrypt password for " + nexusServerId, sde);
            }

            try
            {
                URI uri = new URI(nexusUrl);

                AuthScope scope = new AuthScope(uri.getHost(), uri.getPort());

                BasicCredentialsProvider bcp = new BasicCredentialsProvider();
                bcp.setCredentials(scope, new UsernamePasswordCredentials(server.getUsername(), password));
                return bcp;
            }
            catch (URISyntaxException e)
            {
                throw new MojoExecutionException("Bad Nexus URL: " + nexusUrl, e);
            }
        }
        else
        {
            return HttpAuthentication.fromConsole();
        }
    }

    UpgradeAdvice getUpgradeAdvice()
    {
        return UpgradeAdvice.fromResources().requireFinal(requireFinal);
    }

    HttpCache newHttpCache() throws MojoExecutionException
    {
        return new VerySimpleCache(opupCache, getNexusCredentials());
    }

    VersionClient getVersionClient() throws MojoExecutionException
    {
        VersionClient versionClient;

        if (nexusUrl != null)
        {
            versionClient = new NexusVersionClient(newHttpCache(), nexusUrl);
        }
        else if (artifactoryUrl != null)
        {
            versionClient = new ArtifactoryVersionClient(newHttpCache(), forServer, artifactoryUrl, artifactoryApiKey);
        }
        else
        {
            getLog().info("nexus.url not specified; using Maven to resolve artifacts");
            versionClient = new MavenArtifactVersionClient(repositoryMetadataManager, localRepository, remoteRepositories);
        }

        return VersionClientUtil.filtered(versionClient, getUpgradeAdvice().versionOverrides());
    }
}

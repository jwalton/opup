package com.atlassian.maven.plugins.opup.mojo;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.atlassian.labs.opup.task.CreateUpgradeBranchesTask;
import com.atlassian.labs.opup.task.MergeTask;
import com.atlassian.labs.opup.task.ShowResultsTask;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "upgrade", aggregator = true)
public class UpgradeMojo extends AbstractOpUpMojo
{
    @Parameter(defaultValue = "${project.build.directory}/opup/working-git-clone")
    public File repositoryClone;

    @Parameter(defaultValue = "${project.build.directory}/opup")
    public File opupDirectory;

    @Parameter(defaultValue = "${project.build.directory}/opup/opup-log/build-pass-fail")
    public File passFailFile;

    @Parameter(defaultValue = "opup/opup-log")
    public String relativeLogDirectory;

    @Parameter(defaultValue = "${project.build.directory}/opup-results.html")
    public File resultsFile;

    @Parameter(property = "buildCommand", defaultValue = "mvn clean test")
    public String buildCommand;

    void logOutput(Process p) throws IOException, InterruptedException
    {
        Thread err = new Thread(new InputStreamLogger(p.getErrorStream()));
        Thread out = new Thread(new InputStreamLogger(p.getInputStream()));

        err.start();
        out.start();

        err.join();
        out.join();
    }

    void exec(String[] cmd) throws IOException
    {
        try
        {
            Process p = Runtime.getRuntime().exec(cmd);

            logOutput(p);

            if (p.waitFor() != 0)
            {
                throw new IOException("Failed to clone Git repository into " + repositoryClone);
            }
        }
        catch (InterruptedException ie)
        {
            Thread.currentThread().interrupt();
        }

    }

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        /* Install the scripts */
        try
        {
            installScript("build-and-log.sh");
            installScript("find-success.sh");
        }
        catch (IOException ioe)
        {
            throw new MojoExecutionException("Failed to create scripts", ioe);
        }

        /* Clone the current repository */
        String[] cloneCmd = {
                "git", "clone", ".", repositoryClone.getPath()
        };

        try
        {
            exec(cloneCmd);
        }
        catch (IOException ioe)
        {
            throw new MojoExecutionException("Failed to clone Git repository", ioe);
        }

        /* Push the current branch to 'master' */
        String[] pushRefCmd = {
                "git", "push", repositoryClone.getPath(), "HEAD:master"
        };

        try
        {
            exec(pushRefCmd);
        }
        catch (IOException ioe)
        {
            throw new MojoExecutionException("Failed to push current branch to master in working clone", ioe);
        }

        CreateUpgradeBranchesTask task = new CreateUpgradeBranchesTask(strategy, newHttpCache(), getUpgradeAdvice(), getVersionClient(), repositoryClone);
        task.generateUpgradeBranches(reportFile, getLog());

        getLog().info("OpUp report: " + "file://" + reportFile);

        String[] findSuccessCmd = {
                new File(opupDirectory, "bin/find-success.sh").getAbsolutePath(),
                repositoryClone.getAbsolutePath(),
                buildCommand,
                passFailFile.getParentFile().getAbsolutePath()
        };


        // Run the script
        try
        {
            exec(findSuccessCmd);
        }
        catch (IOException ioe)
        {
            throw new MojoExecutionException("Failed to run find-success.sh", ioe);
        }

        MergeTask mt = MergeTask.from(repositoryClone,
                new File(opupDirectory, "commit.keys"),
                task.poms(),
                new File(opupDirectory, "opup-log/upgrade-refs"));

        mt.merge();

        ShowResultsTask resultsTask = new ShowResultsTask(opupDirectory, passFailFile, relativeLogDirectory);

        resultsTask.generateResults(resultsFile);

        getLog().info("OpUp upgrade results: " + "file://" + resultsFile);

        String[] fetchCmd = {
                "git", "fetch", "-f", repositoryClone.getPath(), "master:opup/upgrade"
        };

        try
        {
            exec(fetchCmd);
        }
        catch (IOException ioe)
        {
            throw new MojoExecutionException("Failed to fetch upgrade branch", ioe);
        }
    }

    private void installScript(String name) throws IOException
    {
        InputStream in = getClass().getResourceAsStream(name);
        if (in == null)
        {
            throw new IOException("Missing resource: " + name);
        }

        File binDir = new File(opupDirectory, "bin");
        if (!binDir.isDirectory())
        {
            binDir.mkdirs();
        }

        File target = new File(binDir, name);
        FileUtils.copyInputStreamToFile(in, target);

        if (!target.setExecutable(true))
        {
            throw new IOException("Failed to make script executable: " + target);
        }
    }

    private class InputStreamLogger implements Runnable
    {
        private final BufferedReader br;
        private final Log log;

        public InputStreamLogger(InputStream in)
        {
            this.br = new BufferedReader(new InputStreamReader(in));
            log = getLog();
        }

        @Override
        public void run()
        {
            try
            {
                String s;
                while ((s = br.readLine()) != null)
                {
                    log.info(s);
                }
            }
            catch (IOException ioe)
            {
                log.error("Failed to read from external command", ioe);
            }
        }
    }
}
